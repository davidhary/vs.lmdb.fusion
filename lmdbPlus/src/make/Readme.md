# How to build native libs and applications

## Buidling LMDB Plus for Windows
* change line 30 in Makefile `all: $(SHARED_W)` 
* run (double-click) `run-make` 

## Buidling LMDB Plus for Linux
* change line 30 in Makefile `all: $(SHARED_L)` 
* Run `make`

## Buidling LMDB Utilities
* make a copy of `liblmdb`
* name the folder `liblmdbmake`
* run `LmdbAppsMake`
* run `LmdbAppsCopy` to copy the applications to the `..\..\..\Apps` folder

