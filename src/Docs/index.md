# isr.Lmdb.Fusion Documentation

A .NET wrapper for  OpenLDAP's [LMDB](https://github.com/LMDB/lmdb) key-value store.
Provides a .NET/C# friendly API and supports zero-copy access to the native library.

## Documentation

[Symas: LMDB Technical Information](https://symas.com/lmdb/technical/)
[Symas: Understanding the LMDB Database File Size and Mewmorey Utiliation](https://symas.com/understanding-lmdb-database-file-sizes-and-memory-utilization/)  
[Lightning Memory-Mapped Database Manager (LMDB)](http://www.lmdb.tech/doc/) 
[Python binding for the LMDB ‘Lightning’ Database](https://lmdb.readthedocs.io/en/latest/)

## Overview

#### Create a database

```c#
    var path = Path.GetFullPath( Path.Combine( "c:/users/public/documents", "fusion" ) );
    using LmdbEnvironment env = new LmdbEnvironment();
    env.Open( path );
    Database dbase;
	using var tx = env.BeginOpenDatabaseTransaction( TransactionBeginOptions.None )
	dbase = tx.OpenDatabase( "db", new DatabaseConfiguration(DatabaseOptions.Create) );
	tx.Commit();
    // use dbase from here on
```

#### Simple Store and Retrieve

```c#
<env points to an open LmdbEnvironment>
<dbase points to an open Lmdb Database>
...  
	int key = 234;
	var keyBuf = BitConverter.GetBytes(key);
	string putData = "Test Data";
	{
		using var tx = env.BeginTransaction(TransactionModes.None);
		_ = dbase.Put(tx, keyBuf, Encoding.UTF8.GetBytes(putData), PutOptions.None);
		tx.Commit();
	}

	ReadOnlySpan<byte> getData;
	{
		using var tx = env.BeginReadOnlyTransaction(TransactionModes.None);
		_ = dbase.Get(tx, keyBuf, out getData);
		tx.Commit();
	}
	Assert.Equal(putData, Encoding.UTF8.GetString(getData));
```

#### Cursor Operations - Single-Value Database

```c#
<Dbase points to an open Lmdb Database, tx is an open Lmdb transaction>
...
	// basic iteration
	{
		using var cursor = Dbase.OpenCursor(tx);
		foreach (var entry in cursor.Forward) {  // cursor.Reverse goes the other way
			var key = BitConverter.ToInt32(entry.Key);
			var data = Encoding.UTF8.GetString(entry.Data);
		}
	}

	// move cursor to key position and get data forward from that key
	{
		using var cursor = Dbase.OpenCursor(tx);
		var keyBytes = BitConverter.GetBytes(1874);
		if (cursor.MoveToKey(keyBytes)) {
			_ = cursor.GetCurrent(out KeyDataPair entry));
			var dataString = Encoding.UTF8.GetString(entry.Data);
			while (cursor.GetNext(...)) {
				//
			}
		}
	}
	
	// iterate over key range (using foreach)
	{
		using var cursor = Dbase.OpenCursor(tx);
		var startKeyBytes = BitConverter.GetBytes(33);
		Assert.True(cursor.MoveToKey(startKeyBytes));

		var endKeyBytes = BitConverter.GetBytes(99);
		foreach (var entry in cursor.ForwardFromCurrent) {
			// test for end of range (> 0 or >=0)
			if (Dbase.Compare(tx, entry.Key, endKeyBytes) > 0)
				break;
			var ckey = BitConverter.ToInt32(entry.Key);
			var cdata = Encoding.UTF8.GetString(entry.Data);
			Console.WriteLine($"{ckey}: {cdata}");
		}
	}

```
#### Cursor Operations - Multi-Value Database
```c#
	// iteration over multi-value database
	{
		using (var cursor = Dbase.OpenMultiValueCursor(tx);
		foreach (var keyEntry in cursor.ForwardByKey) {
			var key = BitConverter.ToInt32(keyEntry.Key);
			var valueList = new List<string>();
			// iterate over the values in the same key
			foreach (var value in cursor.ValuesForward) {
				var data = Encoding.UTF8.GetString(value);
				valueList.Add(data);
			}
		}
	}

	// move to key, iterate over multiple values for key
	{
		using (var cursor = Dbase.OpenMultiValueCursor(tx);
		_ = cursor.MoveToKey(BitConverter.GetBytes(234);
		var valueList = new List<string>();
		foreach (var value in cursor.ValuesForward) {
			var data = Encoding.UTF8.GetString(value);
			valueList.Add(data);
		}
	}

	// Move to key *and* nearest data in multi-value database
	{
		using (var cursor = Dbase.OpenMultiValueCursor(tx);
		var dataBytes = Encoding.UTF8.GetBytes("Test Data");
		var keyData = new KeyDataPair(BitConverter.GetBytes(4), dataBytes);
		KeyDataPair entry;  // the key-value pair nearest to keyData
		_ = cursor.GetNearest(keyData, out entry);
		var dataString = Encoding.UTF8.GetString(entry.Data);
	}
```

The unit tests have more examples, especially for cursor operations.

