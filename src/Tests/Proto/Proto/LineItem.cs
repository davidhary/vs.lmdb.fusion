using System;

namespace isr.Lmdb.Fusion.Proto.Tests
{

    /// <summary>   A line item. </summary>
    /// <remarks>   Remark added by David, 2020-12-11. </remarks>
    public class LineItem
    {
        /// <summary>   Gets or sets the key. </summary>
        /// <value> The key. </value>
        public LineItemKey Key { get; set; }

        /// <summary>   Gets or sets the quantity. </summary>
        /// <value> The quantity. </value>
        public int Quantity { get; set; }

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="obj">  The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as LineItem );
        }

        /// <summary>   Tests if this LineItem is considered equal to another. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="other">    The line item to compare to this object. </param>
        /// <returns>   True if the objects are considered equal, false if they are not. </returns>
        public bool Equals( LineItem other )
        {
            return object.Equals( this.Key, other.Key ) && this.Quantity == other.Quantity;
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return (this.Key?.GetHashCode() ?? 0) ^ this.Quantity.GetHashCode();
        }
    }

}
