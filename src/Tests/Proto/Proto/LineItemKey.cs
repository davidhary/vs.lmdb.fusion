using System;

namespace isr.Lmdb.Fusion.Proto.Tests
{

    /// <summary>   A line item key. </summary>
    /// <remarks>   Remark added by David, 2020-12-11. </remarks>
    public class LineItemKey
    {

        /// <summary>   Gets or sets the product code. </summary>
        /// <value> The product code. </value>
        public string ProdCode { get; set; }

        /// <summary>   Gets or sets the identifier of the order. </summary>
        /// <value> The identifier of the order. </value>
        public int OrderId { get; set; }

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="obj">  The object to compare with the current object. </param>
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as LineItemKey );
        }

        /// <summary>   Tests if this LineItemKey is considered equal to another. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="other">    The line item key to compare to this object. </param>
        /// <returns>   True if the objects are considered equal, false if they are not. </returns>
        public bool Equals( LineItemKey other )
        {
            return string.Equals( this.ProdCode, other.ProdCode, StringComparison.Ordinal ) && this.OrderId == other.OrderId;
        }

        /// <summary>   Serves as the default hash function. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return (this.ProdCode?.GetHashCode( StringComparison.Ordinal ) ?? 0) ^ this.OrderId.GetHashCode();
        }
    }

}
