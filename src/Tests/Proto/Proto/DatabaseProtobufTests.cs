using System;

using Google.Protobuf;

using Xunit;
using Xunit.Abstractions;


namespace isr.Lmdb.Fusion.Proto.Tests.Proto
{

    /// <summary>   A database Protocol Buffer tests. </summary>
    /// <remarks>   Remark added by David, 2020-12-11. </remarks>
    [Collection( nameof( FolderCollection ) )]
    public class DatabaseProtobufTests : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Gets the folder fixture. </summary>
        /// <value> The folder fixture. </value>
        private FolderFixture FolderFixture { get; set; }

        /// <summary>   The output. </summary>
        private readonly ITestOutputHelper _Output;

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="output">   The output. </param>
        public DatabaseProtobufTests( FolderFixture folderFixture, ITestOutputHelper output )
        {
            this.FolderFixture = folderFixture;
            this._Output = output;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="disposing">    True to release both managed and unmanaged resources; false to
        ///                             release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            if ( disposing )
                this.FolderFixture = null;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

        /// <summary>   (Unit Test Method) Could acquire return. </summary>
        /// <remarks>   David, 2021-01-25. </remarks>
        [Fact]
        public void CouldAcquireReturn()
        {
            BufferPool buffers = new BufferPool();
            var buffer = buffers.Acquire( 1024 );
            buffers.Return( buffer );
        }

        /// <summary>   (Unit Test Method) Could create database. </summary>
        /// <remarks>   David, 2021-01-25. </remarks>
        [Fact]
        public void CouldCreateDatabase()
        {

            var config = new DatabaseConfiguration( DatabaseOpenOptions.Create );
            Database dbase;
            var path = this.FolderFixture.CreateDirectoryForTest();
            using var env = LmdbEnvironment.OpenNew( path );

            using ( var tx = env.BeginOpenDatabaseTransaction( TransactionBeginOptions.None ) )
            {
                dbase = tx.OpenDatabase( "TestDb2", config );
                tx.Commit();
            }

        }

        /// <summary>   (Unit Test Method) Could store key value. </summary>
        /// <remarks>   David, 2021-01-25. </remarks>
        [Fact]
        public void CouldStoreKeyValue()
        {

            var config = new DatabaseConfiguration( DatabaseOpenOptions.Create );
            Database dbase;
            var path = this.FolderFixture.CreateDirectoryForTest();
            using var env = LmdbEnvironment.OpenNew( path );

            using ( var tx = env.BeginOpenDatabaseTransaction( TransactionBeginOptions.None ) )
            {
                dbase = tx.OpenDatabase( "TestDb2", config );
                tx.Commit();
            }

            var lik = new LineItemKey { OrderId = 1, ProdCode = "GIN" };
            var li = new LineItem { Key = lik, Quantity = 8 };

            BufferPool buffers = new BufferPool();

            var buffer = buffers.Acquire( 1024 );
            try
            {
                using ( var tx = env.BeginTransaction( TransactionBeginOptions.None ) )
                {
                    var outStream = new CodedOutputStream( buffer );
                    lik.WriteTo( outStream );
                    var likPos = ( int ) outStream.Position;
                    li.WriteTo( outStream );
                    var liPos = ( int ) outStream.Position;
                    var keySpan = new ReadOnlySpan<byte>( buffer, 0, likPos );
                    var dataSpan = new ReadOnlySpan<byte>( buffer, likPos, liPos - likPos );
                    _ = dbase.Put( tx, keySpan, dataSpan, DatabasePutOptions.None );
                    tx.Commit();
                }
            }
            finally
            {
                buffers.Return( buffer );
            }
        }

        /// <summary>   (Unit Test Method) Could store retrieve. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        [Fact]
        public void CouldStoreRetrieve() 
        {

            var config = new DatabaseConfiguration( DatabaseOpenOptions.Create );
            Database dbase;
            var path = this.FolderFixture.CreateDirectoryForTest();
            using var env = LmdbEnvironment.OpenNew( path ); 

            using ( var tx = env.BeginOpenDatabaseTransaction( TransactionBeginOptions.None ) )
            {
                dbase = tx.OpenDatabase( "TestDb2", config );
                tx.Commit();
            }

            var lik = new LineItemKey { OrderId = 1, ProdCode = "GIN" };
            var li = new LineItem { Key = lik, Quantity = 8 };
            var lik2 = new LineItemKey { OrderId = 2, ProdCode = "WHISKY" };
            var li2 = new LineItem { Key = lik2, Quantity = 24 };

            BufferPool buffers = new BufferPool();

            var buffer = buffers.Acquire( 1024 );
            try
            {
                using ( var tx = env.BeginTransaction( TransactionBeginOptions.None ) )
                {
                    var outStream = new CodedOutputStream( buffer );
                    lik.WriteTo( outStream );
                    var likPos = ( int ) outStream.Position;
                    li.WriteTo( outStream );
                    var liPos = ( int ) outStream.Position;
                    var keySpan = new ReadOnlySpan<byte>( buffer, 0, likPos );
                    var dataSpan = new ReadOnlySpan<byte>( buffer, likPos, liPos - likPos );
                    _ = dbase.Put( tx, keySpan, dataSpan, DatabasePutOptions.None );

                    // CodedOutputStream instances cannot be reused
                    outStream = new CodedOutputStream( buffer );
                    lik2.WriteTo( outStream );
                    likPos = ( int ) outStream.Position;
                    li2.WriteTo( outStream );
                    liPos = ( int ) outStream.Position;
                    keySpan = new ReadOnlySpan<byte>( buffer, 0, likPos );
                    dataSpan = new ReadOnlySpan<byte>( buffer, likPos, liPos - likPos );
                    _ = dbase.Put( tx, keySpan, dataSpan, DatabasePutOptions.None );
                    tx.Commit();
                }

                LineItem liOut;
                LineItem liOut2;
                using ( var tx = env.BeginTransaction( TransactionBeginOptions.None ) )
                {
                    var keyStream = new CodedOutputStream( buffer );
                    lik.WriteTo( keyStream );
                    var likPos = ( int ) keyStream.Position;
                    var keySpan = new ReadOnlySpan<byte>( buffer, 0, likPos );
                    Assert.True( dbase.Get( tx, keySpan, out ReadOnlySpan<byte> dataSpan ) );
                    var dataBytes = dataSpan.ToArray();
                    liOut = LineItem.Parser.ParseFrom( dataBytes );

                    keyStream = new CodedOutputStream( buffer );
                    lik2.WriteTo( keyStream );
                    likPos = ( int ) keyStream.Position;
                    keySpan = new ReadOnlySpan<byte>( buffer, 0, likPos );
                    Assert.True( dbase.Get( tx, keySpan, out dataSpan ) );
                    liOut2 = LineItem.Parser.ParseFrom( dataSpan.ToArray() );

                    tx.Commit();
                }

                Assert.True( li.Equals( liOut ) );
                Assert.True( li2.Equals( liOut2 ) );
            }
            finally
            {
                buffers.Return( buffer );
                using var tx = env.BeginTransaction( TransactionBeginOptions.None );
                _ = dbase.Drop( tx );
                tx.Commit();
            }
        }

        /// <summary>   (Unit Test Method) Could store key value span. </summary>
        /// <remarks>   David, 2021-01-25. </remarks>
        [Fact]
        public void CouldStoreKeyValueSpan()
        {

            var config = new DatabaseConfiguration( DatabaseOpenOptions.Create );
            Database dbase;
            var path = this.FolderFixture.CreateDirectoryForTest();
            using var env = LmdbEnvironment.OpenNew( path );

            using ( var tx = env.BeginOpenDatabaseTransaction( TransactionBeginOptions.None ) )
            {
                dbase = tx.OpenDatabase( "TestDb2", config );
                tx.Commit();
            }

            var lineItemKey = new LineItemKey { OrderId = 1, ProdCode = "GIN" };
            var lineItemValue = new LineItem { Key = lineItemKey, Quantity = 8 };

            try
            {
                using var tx = env.BeginTransaction( TransactionBeginOptions.None );
                Span<byte> keySpan = new Span<byte>( new byte[lineItemKey.CalculateSize()] );
                lineItemKey.WriteTo( keySpan );
                Span<byte> dataSpan = new Span<byte>( new byte[lineItemValue.CalculateSize()] );
                lineItemValue.WriteTo( dataSpan );
                _ = dbase.Put( tx, keySpan, dataSpan, DatabasePutOptions.None );
                tx.Commit();
            }
            finally
            {
            }
        }

        /// <summary>   (Unit Test Method) Could store retrieve spans. </summary>
        /// <remarks>   David, 2021-01-25. </remarks>
        [Fact]
        public void CouldStoreRetrieveSpans()
        {

            var config = new DatabaseConfiguration( DatabaseOpenOptions.Create );
            Database dbase;
            var path = this.FolderFixture.CreateDirectoryForTest();
            using var env = LmdbEnvironment.OpenNew( path );

            using ( var tx = env.BeginOpenDatabaseTransaction( TransactionBeginOptions.None ) )
            {
                dbase = tx.OpenDatabase( "TestDb2", config );
                tx.Commit();
            }

            var lineItemKey1 = new LineItemKey { OrderId = 1, ProdCode = "GIN" };
            var lineItemValue1 = new LineItem { Key = lineItemKey1, Quantity = 8 };
            var lineItemKey2 = new LineItemKey { OrderId = 2, ProdCode = "WHISKY" };
            var lineItemValue2 = new LineItem { Key = lineItemKey2, Quantity = 24 };

            try
            {
                {
                    using var tx = env.BeginTransaction( TransactionBeginOptions.None );
                    Span<byte> keySpan = new Span<byte>( new byte[lineItemKey1.CalculateSize()] );
                    lineItemKey1.WriteTo( keySpan );
                    Span<byte> dataSpan = new Span<byte>( new byte[lineItemValue1.CalculateSize()] );
                    lineItemValue1.WriteTo( dataSpan );
                    _ = dbase.Put( tx, keySpan, dataSpan, DatabasePutOptions.None );

                    keySpan = new Span<byte>( new byte[lineItemKey2.CalculateSize()] );
                    lineItemKey2.WriteTo( keySpan );
                    dataSpan = new Span<byte>( new byte[lineItemValue2.CalculateSize()] );
                    lineItemValue2.WriteTo( dataSpan );
                    _ = dbase.Put( tx, keySpan, dataSpan, DatabasePutOptions.None );

                    tx.Commit();
                }

                LineItem lineItemValue1Out = new LineItem();
                LineItem lineItemValue2Out = new LineItem();

                {
                    using var tx = env.BeginTransaction( TransactionBeginOptions.None );
                    Span<byte> keySpan = new Span<byte>( new byte[lineItemKey1.CalculateSize()] );
                    lineItemKey1.WriteTo( keySpan );
                    Assert.True( dbase.Get( tx, keySpan, out ReadOnlySpan<byte> dataSpan ) );
                    lineItemValue1Out.MergeFrom( dataSpan.ToArray() );

                    keySpan = new Span<byte>( new byte[lineItemKey2.CalculateSize()] );
                    lineItemKey2.WriteTo( keySpan );
                    Assert.True( dbase.Get( tx, keySpan, out dataSpan ) );
                    lineItemValue2Out.MergeFrom( dataSpan.ToArray() );

                    tx.Commit();
                }

                Assert.True( lineItemValue1.Equals( lineItemValue1Out ) );
                Assert.True( lineItemValue2.Equals( lineItemValue2Out ) );
            }
            finally
            {
                using var tx = env.BeginTransaction( TransactionBeginOptions.None );
                _ = dbase.Drop( tx );
                tx.Commit();
            }
        }

        /// <summary>   Could store retrieve spans 1. </summary>
        /// <remarks>   David, 2021-01-25. </remarks>
        [Fact]
        public void CouldStoreRetrieveSpans1()
        {

            var config = new DatabaseConfiguration( DatabaseOpenOptions.Create );
            Database dbase;
            var path = this.FolderFixture.CreateDirectoryForTest();
            using var env = LmdbEnvironment.OpenNew( path );

            using ( var tx = env.BeginOpenDatabaseTransaction( TransactionBeginOptions.None ) )
            {
                dbase = tx.OpenDatabase( "TestDb2", config );
                tx.Commit();
            }

            var lineItemKey1 = new LineItemKey { OrderId = 1, ProdCode = "GIN" };
            var lineItemValue1 = new LineItem { Key = lineItemKey1, Quantity = 8 };
            var lineItemKey2 = new LineItemKey { OrderId = 2, ProdCode = "WHISKY" };
            var lineItemValue2 = new LineItem { Key = lineItemKey2, Quantity = 24 };

            try
            {
                {
                    using var tx = env.BeginTransaction( TransactionBeginOptions.None );
                    Span<byte> keySpan = new Span<byte>( new byte[lineItemKey1.CalculateSize()] );
                    lineItemKey1.WriteTo( keySpan );
                    Span<byte> dataSpan = new Span<byte>( new byte[lineItemValue1.CalculateSize()] );
                    lineItemValue1.WriteTo( dataSpan );
                    _ = dbase.Put( tx, keySpan, dataSpan, DatabasePutOptions.None );

                    keySpan = new Span<byte>( new byte[lineItemKey2.CalculateSize()] );
                    lineItemKey2.WriteTo( keySpan );
                    dataSpan = new Span<byte>( new byte[lineItemValue2.CalculateSize()] );
                    lineItemValue2.WriteTo( dataSpan );
                    _ = dbase.Put( tx, keySpan, dataSpan, DatabasePutOptions.None );

                    tx.Commit();
                }

                LineItem lineItemValue1Out = new LineItem();
                LineItem lineItemValue2Out = new LineItem();

                {
                    using var tx = env.BeginTransaction( TransactionBeginOptions.None );
                    Span<byte> keySpan = new Span<byte>( new byte[lineItemKey1.CalculateSize()] );
                    lineItemKey1.WriteTo( keySpan );
                    Assert.True( dbase.Get( tx, keySpan, out ReadOnlySpan<byte> dataSpan ) );
                    lineItemValue1Out.MergeFrom( dataSpan.ToArray() );

                    keySpan = new Span<byte>( new byte[lineItemKey2.CalculateSize()] );
                    lineItemKey2.WriteTo( keySpan );
                    Assert.True( dbase.Get( tx, keySpan, out dataSpan ) );
                    lineItemValue2Out.MergeFrom( dataSpan.ToArray() );

                    tx.Commit();
                }

                Assert.True( lineItemValue1.Equals( lineItemValue1Out ) );
                Assert.True( lineItemValue2.Equals( lineItemValue2Out ) );
            }
            finally
            {
                using var tx = env.BeginTransaction( TransactionBeginOptions.None );
                _ = dbase.Drop( tx );
                tx.Commit();
            }
        }

    }
}
