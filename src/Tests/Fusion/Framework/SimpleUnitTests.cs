using System;

using Xunit;

namespace isr.Lmdb.Fusion.Tests.Framework
{
    /// <summary>   Simple unit tests for project validation. </summary>
    /// <remarks>   David, 2020-12-14. 
    /// Use to validate project settings. </remarks>
    public class SimpleUnitTests
    {

        /// <summary>   (unit test method) Tests passing. </summary>
        /// <remarks>   David, 2020-12-14. </remarks>
        [Fact]
        public void PassingTest()
        {
            Assert.Equal( 4, SimpleUnitTests.Add( 2, 2 ) );
        }

        /// <summary>   (unit test method) Tests throwing an <see cref="AssertX"/> <see cref="EqualsException"/>. </summary>
        /// <remarks>   David, 2020-12-14. </remarks>
        [Fact]
        public void FailingTest()
        {
            string message = "This test should fail.";
            void act() => AssertX.Equal( 5, SimpleUnitTests.Add( 2, 2 ), message );
            EqualsException exception = Assert.Throws<EqualsException>( act );
            Assert.Equal( message, exception.Message.Split("\n")[0] );
        }

        /// <summary>   Adds x. </summary>
        /// <remarks>   David, 2020-12-14. </remarks>
        /// <param name="x">    The x coordinate. </param>
        /// <param name="y">    The y coordinate. </param>
        /// <returns>   An int. </returns>
        private static int Add( int x, int y )
        {
            return x + y;
        }

        /// <summary>   (unit test method) Odd even theory. </summary>
        /// <remarks>   David, 2020-12-23. </remarks>
        /// <param name="value">    The value. </param>
        [Theory]
        [InlineData( 3 )]
        [InlineData( 5 )]
        [InlineData( 7 )]
        [CLSCompliant( false )]
        public void OddEvenTheory( int value )
        {
            Assert.True( IsOdd( value ) );
        }

        /// <summary>   Query if 'value' is odd. </summary>
        /// <remarks>   David, 2020-12-14. </remarks>
        /// <param name="value">    The value. </param>
        /// <returns>   True if odd, false if not. </returns>
        private static bool IsOdd( int value )
        {
            return value % 2 == 1;
        }

#if false
        private string Bar { get; }

        [Fact]
        public void WriteReadOnlyField()
        {
            _Bar = "Bar";
        }
#endif
    }
}
