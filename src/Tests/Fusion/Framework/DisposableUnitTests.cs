using System;
using Xunit;

namespace isr.Lmdb.Fusion.Tests.Framework
{
    /// <summary>   Disposable unit tests for studying how Using works. </summary>
    /// <remarks>   David, 2020-12-14. </remarks>
    public class DisposableUnitTests
    {

        private class DisposableObject : IDisposable
        {

            public bool Disposed { get; set; }

            protected virtual void Dispose( bool disposing )
            {
                if ( disposing )
                    this.Disposed = true;
            }

            public void Dispose()
            {
                this.Dispose( true );
            }
        }


        /// <summary>   (unit test method) Can try catch finally dispose. </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        [Fact]
        public void CanTryCatchFinallyDispose()
        {
            DisposableObject Target = new DisposableObject();
            try
            {
                Assert.False( Target.Disposed, $"{nameof( DisposableObject )}.{nameof( DisposableObject.Disposed )}" );
            }
            catch ( Exception )
            {

                throw;
            }
            finally
            {
                ((IDisposable)Target)?.Dispose();
            }
            Assert.True( Target.Disposed , $"{nameof( DisposableObject )}.{nameof( DisposableObject.Disposed )}" );
        }

        /// <summary>   Can using block dispose. </summary>
        /// <remarks>   David, 2021-01-12. <para>
        /// This seems to contradict the 
        /// <see cref="https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/language-specification/statements#the-using-statement">C# documentation</see>/> 
        /// </para> </remarks>
        [Fact]
        public void CanUsingBlockDispose()
        {
            using DisposableObject Target = new DisposableObject();
            {
                Assert.False( Target.Disposed, $"{nameof( DisposableObject )}.{nameof( DisposableObject.Disposed )}" );
            }
            bool disposed = Target.Disposed;
            Assert.False( Target.Disposed, $"Using block not disposing {nameof( DisposableObject )}.{nameof( DisposableObject.Disposed )}" );
        }

    }
}
