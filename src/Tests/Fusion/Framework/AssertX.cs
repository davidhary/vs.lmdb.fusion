using Xunit.Extensions;

namespace isr.Lmdb.Fusion.Tests
{
    /// <summary>   Exception for signaling equality errors. </summary>
    /// <remarks>   GRHM, 2018-10-24. <para>
    /// https://stackoverflow.com/users/204690/grhm </para><para>
    /// https://stackoverflow.com/questions/42203169/how-to-implement-xunit-descriptive-assert-message </para> </remarks>
    public class EqualsException : Xunit.Sdk.EqualException
    {
        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-12-22. </remarks>
        /// <param name="expected">     The expected. </param>
        /// <param name="actual">       The actual. </param>
        /// <param name="userMessage">  A user generated message describing the assertion. </param>
        public EqualsException( object expected, object actual, string userMessage ) : base( expected, actual )
        {
            this.UserMessage = userMessage;
        }

        /// <summary>   Gets the message. </summary>
        /// <value> The message. </value>
        public override string Message => this.UserMessage + "\n" + base.Message;
    }

    /// <summary>   An assert x coordinate. </summary>
    /// <remarks>   GRHM, 2018-10-24. <para>
    /// https://stackoverflow.com/users/204690/grhm </para><para>
    /// https://stackoverflow.com/questions/42203169/how-to-implement-xunit-descriptive-assert-message </para> </remarks>
    public static class AssertX
    {
        /// <summary>   Verifies that two objects are equal, using a default comparer. </summary>
        /// <remarks>   Remarked by David, 2020-12-22. </remarks>
        /// <exception cref="EqualsException"> Thrown when the objects are not equal. </exception>
        /// <typeparam name="T">    The type of the objects to be compared. </typeparam>
        /// <param name="expected">     The expected value. </param>
        /// <param name="actual">       The value to be compared against. </param>
        /// <param name="userMessage">  Message to show in the error. </param>
        public static void Equal<T>( T expected, T actual, string userMessage )
        {
            bool areEqual;

            if ( expected == null || actual == null )
            {
                // If either null, equal only if both null
                areEqual = (expected == null && actual == null);
            }
            else
            {
                // expected is not null - so safe to call .Equals()
                areEqual = expected.Equals( actual );
            }

            if ( !areEqual )
            {
                throw new EqualsException( expected, actual, userMessage );
            }
        }
    }

}
