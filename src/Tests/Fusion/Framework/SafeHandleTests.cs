using System;

using Xunit;
using Xunit.Abstractions;

namespace isr.Lmdb.Fusion.Tests.Framework
{
    /// <summary>   Safe Handle tests. </summary>
    /// <remarks>   David, 2020-12-14. </remarks>
    public class SafeHandleTests
    {

        /// <summary>   The output. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0052:Remove unread private members", Justification = "<Pending>" )]
        private readonly ITestOutputHelper _Output;

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="output">   The output. </param>
        public SafeHandleTests( ITestOutputHelper output )
        {
            this._Output = output;
        }

        private class MySafeHandle : LmdbSafeHandle
        {

            /// <summary>
            /// Constructor that creates a <see cref="System.Runtime.InteropServices.SafeHandle"/> with an invalid handle
            /// and a <see cref="MySafeHandle.ReleaseAction"/>
            /// </summary>
            /// <remarks>   Remarked by David, 2020-12-16. </remarks>
            public MySafeHandle() : base() { }

            /// <summary>
            /// Constructor that creates a <see cref="System.Runtime.InteropServices.SafeHandle"/> with a
            /// handle and a <see cref="MySafeHandle.ReleaseAction"/>
            /// </summary>
            /// <remarks>   Remarked by David, 2020-12-16. </remarks>
            /// <param name="handle">   The handle. </param>
            public MySafeHandle( IntPtr handle ) : base( handle ) { }

            private TransactionState _State;
            /// <summary>   Current transaction state. </summary>
            /// <value> The state. </value>
            internal TransactionState State
            {
                get => this._State;
                set {
                    this._State = value;
                    if ( TransactionState.Committed == value || TransactionState.Aborted == value )
                    {
                        this.Freed = true;
                    }
                }
            }

            /// <summary>   Frees the allocated handle. </summary>
            /// <remarks>   David, 2021-01-11. </remarks>
            protected override void Free()
            {
                // tag the handle as free.
                base.Free();
            }
        }

        /// <summary>   ( unit test method ) Safe Handle should not be invalid. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        [Fact]
        public void SafeHandleShouldNotBeInvalid()
        {
            MySafeHandle safeHandle = new MySafeHandle( new IntPtr( 1 ) );
            Assert.False( safeHandle.IsInvalid );
        }

        /// <summary>   ( unit test method ) Safe handle should be allocated. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        [Fact]
        public void SafeHandleShouldBeAllocated()
        {
            MySafeHandle safeHandle = new MySafeHandle( new IntPtr( 1 ) );
            Assert.True( safeHandle.IsAllocated );
        }

        /// <summary>   ( unit test method ) Safe handle should not be allocated. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        [Fact]
        public void SafeHandleShouldNotBeAllocated()
        {
            MySafeHandle safeHandle = new MySafeHandle( new IntPtr( 1 ) ) {
                State = TransactionState.Committed
            };
            Assert.False( safeHandle.IsAllocated );
        }

        /// <summary>   ( unit test method ) Safe handle should be invalid. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        [Fact]
        public void SafeHandleShouldBeInvalid()
        {
            MySafeHandle safeHandle = new MySafeHandle( new IntPtr( 1 ) );
            safeHandle.Dispose();
            Assert.True( safeHandle.IsInvalid );
        }

    }
}
