using System;
using System.Collections.Generic;
using System.Text;

using Xunit;

namespace isr.Lmdb.Fusion.Tests
{
    /// <summary>   A database fixture. </summary>
    /// <remarks>
    /// Remark added by David, 2020-12-11. A single test context to be shared among tests in several
    /// test classes, and cleaned up after all the tests in the test classes have finished. Test data
    /// are populated in a dictionary and stored to the database. The dictionary is accessible for
    /// validations of stored values.
    /// </remarks>
    public class DatabaseFixture : EnvironmentFixture
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>
        /// Remark added by David, 2020-12-11.
        /// <list type="bullet"><listheader>Construction</listheader><item>
        /// Populates the <see cref="TestData"/> dictionary of <see cref="Dictionary{TKey, TValue}"/> of
        /// '{Integer,String[]}</item><item>
        /// 
        /// Opens a new database (create) using the
        /// <see cref="isr.Lmdb.Span.CompareFunctions.CompareInt32(in ReadOnlySpan{byte}, in ReadOnlySpan{byte})"/> key comparison
        /// function and <see cref="isr.Lmdb.Span.CompareFunctions.CompareString(in ReadOnlySpan{byte}, in ReadOnlySpan{byte})"/>
        /// data comparison function</item><item>
        /// 
        /// Stores the dictionary in the database as key value pairs to the dictionary key value
        /// pairs.</item><item>
        /// </item></list>
        /// </remarks>
        public DatabaseFixture()
        {

            this.PopulateDataDictionary();

            this.OpenMultiValueDatabase();

            this.StoreTestData();

        }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Lmdb.KdSoft.Tests.DatabaseFixture and optionally
        /// releases the managed resources.
        /// </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="disposing">    True to release both managed and unmanaged resources; false to
        ///                             release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                if ( this.Env is object )
                {
                    using var tx = this.Env.BeginTransaction( TransactionBeginOptions.None );
                    this.Db.DropThrow( tx );
                    tx.Commit();
                }
            }
            base.Dispose( disposing );
        }

        /// <summary>   Name of the database. </summary>
        public const string DatabaseName = "Test Read Database";

        /// <summary>   Number of first count. </summary>
        public const int FirstCount = 7;

        /// <summary>   The gap. </summary>
        public const int Gap = 12;

        /// <summary>   Number of second count. </summary>
        public const int SecondCount = 5;

        /// <summary>   Opens multi value database. </summary>
        /// <remarks>   David, 2020-12-14. </remarks>
        private void OpenMultiValueDatabase()
        {
            var config = new DatabaseConfiguration( DatabaseOpenOptions.Create | DatabaseOpenOptions.DuplicatesSort, new Interop.LmdbValue(),
                                                    isr.Lmdb.Fusion.Span.CompareFunctions.CompareInt32,
                                                    isr.Lmdb.Fusion.Span.CompareFunctions.CompareStringOrdinal );
            this.Db = this.Env.OpenDatabase( DatabaseFixture.DatabaseName, config );
        }

        /// <summary>   Populates the data dictionary. </summary>
        /// <remarks>   David, 2020-12-14. </remarks>
        private void PopulateDataDictionary()
        {

            this.TestData = new Dictionary<int, IList<string>>();

            // save first count (7) values for keys 0..(first count -1): even ? key : key+a,key+b,....
            for ( int key = 0; key < DatabaseFixture.FirstCount; key++ )
            {
                if ( key % 2 == 0 )
                {  // if even key
                    string putData = $"Test Data {key}";
                    this.TestData[key] = new[] { putData };
                }
                else
                {
                    var putData = new string[key];
                    char dupLetter = 'a';
                    for ( int indx = 0; indx < key; indx++ )
                    {
                        putData[indx] = $"Test Data {key}{dupLetter}";
                        dupLetter++;
                    }
                    this.TestData[key] = putData;
                }
            }

            // save second count (5) values for keys (second start)..(second start + second count -1): even ? key : key+a,key+b,....
            int secondStart = DatabaseFixture.FirstCount + DatabaseFixture.Gap;
            for ( int key = secondStart; key < secondStart + DatabaseFixture.SecondCount; key++ )
            {
                if ( key % 2 == 0 )
                {  // if even key
                    string putData = $"Test Data {key}";
                    this.TestData[key] = new[] { putData };
                }
                else
                {
                    var putData = new string[key];
                    char dupLetter = 'a';
                    for ( int indx = 0; indx < key; indx++ )
                    {
                        putData[indx] = $"Test Data {key}{dupLetter}";
                        dupLetter++;
                    }
                    this.TestData[key] = putData;
                }
            }
        }

        /// <summary>   Stores test data. </summary>
        /// <remarks>   David, 2020-12-14. </remarks>
        private void StoreTestData()
        {

            var buffer = new byte[1024];
            using var tx = this.Env.BeginTransaction( TransactionBeginOptions.None );
            foreach ( var testEntry in this.TestData )
            {
                var key = testEntry.Key;
                var putData = this.TestData[key];
                var keyBytes = BitConverter.GetBytes( key );
                for ( int indx = 0; indx < putData.Count; indx++ )
                {
                    var putDataItem = putData[indx];
                    int byteCount = Encoding.UTF8.GetBytes( putDataItem, 0, putDataItem.Length, buffer, 0 );
                    _ = this.Db.Put( tx, keyBytes, new ReadOnlySpan<byte>( buffer, 0, byteCount ), DatabasePutOptions.None );
                }
            }
            tx.Commit();

        }

        /// <summary>   Gets the key value data to be stored in the database. </summary>
        /// <value> Information describing the test. </value>
        public Dictionary<int, IList<string>> TestData { get; private set; }

        /// <summary>   Gets the database. </summary>
        /// <value> The database. </value>
        public Database Db { get; private set; }

    }

    /// <summary>   Defines the <see cref="DatabaseFixture"/> Collections. </summary>
    /// <remarks>   Remark added by David, 2020-12-11. </remarks>
    [CollectionDefinition( nameof( DatabaseCollection ) )]
    public class DatabaseCollection : ICollectionFixture<DatabaseFixture>
    {
        // This class has no code, and is never created. Its purpose is simply to be
        // the place to apply [CollectionDefinition] and all the ICollectionFixture<> interfaces. 
    }
}
