using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xunit;
using Xunit.Abstractions;

namespace isr.Lmdb.Fusion.Tests
{

    /// <summary>   A cursor tests. </summary>
    /// <remarks>   Remark added by David, 2020-12-11. </remarks>
    [Collection( nameof( DatabaseCollection ) )]
    public class CursorDataFixtureTests
    {
        /// <summary>   The fixture. </summary>
        private readonly DatabaseFixture _Fixture;

        /// <summary>   The output. </summary>
        private readonly ITestOutputHelper _Output;

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="fixture">  The fixture. </param>
        /// <param name="output">   The output. </param>
        public CursorDataFixtureTests( DatabaseFixture fixture, ITestOutputHelper output )
        {
            this._Fixture = fixture;
            this._Output = output;
        }

        /// <summary>   Basic iteration by key. </summary>
        /// <remarks>
        /// Remark added by David, 2020-12-11 <para>
        /// Uses the <see cref="EnvironmentFixture"/> 
        /// and multi-Value <see cref="DatabaseFixture"/>.
        /// Step through the database data using a <see cref="Framework.MultiValueCursor"/>
        /// with <see cref="Cursor.ForwardByKey"/> iteration and compare each first item of the string array data.
        /// </para>
        /// <list type="number"><listheader>Values</listheader><item>
        /// 0: Test Data 0     </item><item>
        /// 1: Test Data 1a    </item><item>
        /// 2: Test Data 2     </item><item>
        /// 3: Test Data 3a    </item><item>
        /// 4: Test Data 4     </item><item>
        /// 5: Test Data 5a    </item><item>
        /// 6: Test Data 6     </item><item>
        /// 19: Test Data 19a  </item><item>
        /// 20: Test Data 20   </item><item>
        /// 21: Test Data 21a  </item><item>
        /// 22: Test Data 22   </item><item>
        /// 23: Test Data 23a  </item><item>
        /// </item></list>
        /// </remarks>
        [Fact]
        public void BasicIterationByKey()
        {
            var getData = new Dictionary<int, IList<string>>();
            using ( var tx = this._Fixture.Env.BeginTransaction( TransactionBeginOptions.None ) )
            {
                using var cursor = Cursor.OpenCursor( this._Fixture.Db, tx );
                foreach ( var entry in cursor.ForwardByKey )
                {
                    var key = BitConverter.ToInt32( entry.Key );
                    var data = Encoding.UTF8.GetString( entry.Data );
                    Assert.Equal( this._Fixture.TestData[key][0], data );
                    getData[key] = new[] { data };
                }
            }
            foreach ( var entry in getData )
            {
                this._Output.WriteLine( $"{entry.Key}: {entry.Value[0]}" );
            }
        }

        /// <summary>   Multi value iteration by key. </summary>
        /// <remarks>
        /// Remark added by David, 2020-12-11 <para>
        /// Uses the <see cref="Fixture"/> <see cref="EnvironmentFixture"/> 
        /// and multi-Value <see cref="DatabaseFixture"/>.
        /// Step through the database data using a <see cref="Framework.MultiValueCursor"/>
        /// with <see cref="Cursor.ForwardByKey"/> iteration and then with 
        /// <see cref="Framework.Cursor.ValuesForward"/> iteration and compare the read data with the expected values.
        /// Iteration through the data uses the Cursor Note that the <see cref="Framework.Cursor.ValuesForward"/>
        /// to iterate though the duplicate records of the key.
        /// </para>
        /// <list type="number"><listheader>Values</listheader><item>
        /// 0: </item><item>
        /// 		Test Data 0 </item><item>
        /// 1: </item><item>
        /// 		Test Data 1a </item><item>
        /// 2: </item><item>
        /// 		Test Data 2 </item><item>
        /// 3: </item><item>
        /// 		Test Data 3a </item><item>
        /// 		Test Data 3b </item><item>
        /// 		Test Data 3c </item><item>
        /// 4: </item><item>
        /// 		Test Data 4 </item><item>
        /// 5: </item><item>
        /// 		Test Data 5a </item><item>
        /// ...
        /// 		Test Data 5e </item><item>
        /// 6: </item><item>
        /// 		Test Data 6 </item><item>
        /// 19: </item><item>
        /// 		Test Data 19a </item><item>
        /// ...
        /// 		Test Data 19s </item><item>
        /// ... </item><item>
        /// 22: </item><item>
        /// 		Test Data 22 </item><item>
        /// 23: </item><item>
        /// 		Test Data 23a </item><item>
        /// .... </item><item>
        /// 		Test Data 23w </item><item>
        /// </remarks>
        [Fact]
        public void MultiValueIterationByKey()
        {
            var getData = new Dictionary<int, IList<string>>();
            using ( var tx = this._Fixture.Env.BeginReadOnlyTransaction() )
            {
                using var cursor = Cursor.OpenCursor( this._Fixture.Db, tx );
                foreach ( var keyEntry in cursor.ForwardByKey )
                {
                    var key = BitConverter.ToInt32( keyEntry.Key );
                    var valueList = new List<string>();
                    foreach ( var value in cursor.ValuesForward )
                    {
                        var data = Encoding.UTF8.GetString( value );
                        valueList.Add( data );
                    }
                    getData[key] = valueList;
                }
            }
            foreach ( var entry in getData )
            {
                this._Output.WriteLine( $"{entry.Key}:" );
                foreach ( var val in entry.Value )
                    this._Output.WriteLine( $"\t\t{val}" );
            }
            Assert.Equal( this._Fixture.TestData, getData );
        }

        /// <summary>   Multi value iteration. </summary>
        /// <remarks>
        /// Remark added by David, 2020-12-11 <para>
        /// Uses the <see cref="Fixture"/> <see cref="EnvironmentFixture"/> 
        /// and multi-Value <see cref="DatabaseFixture"/>.
        /// Steps through the database data using a <see cref="Framework.MultiValueCursor"/>
        /// with <see cref="Cursor.Forward"/> iteration and compare each first item of the string array data.
        /// </para>
        /// <list type="number"><listheader>Values</listheader><item>
        /// 0: Test Data 0 </item><item>
        /// 1: Test Data 1a </item><item>
        /// 2: Test Data 2 </item><item>
        /// 3: Test Data 3a </item><item>
        /// 3: Test Data 3b </item><item>
        /// 3: Test Data 3c </item><item>
        /// .... </item><item>
        /// 23: Test Data 23r </item><item>
        /// 23: Test Data 23s </item><item>
        /// 23: Test Data 23t </item><item>
        /// 23: Test Data 23u </item><item>
        /// 23: Test Data 23v </item><item>
        /// 23: Test Data 23w </item>
        /// </remarks>
        [Fact]
        public void MultiValueIteration()
        {
            var getData = new Dictionary<int, IList<string>>();
            using ( var tx = this._Fixture.Env.BeginReadOnlyTransaction() )
            {
                using var cursor = Cursor.OpenCursor( this._Fixture.Db, tx );
                foreach ( var entry in cursor.Forward )
                {
                    var ckey = BitConverter.ToInt32( entry.Key );
                    var cdata = Encoding.UTF8.GetString( entry.Data );
                    this._Output.WriteLine( $"{ckey}: {cdata}" );
                    if ( getData.TryGetValue( ckey, out IList<string> dataList ) )
                        dataList.Add( cdata );
                    else
                        getData[ckey] = new List<string> { cdata };
                }
            }
            Assert.Equal( this._Fixture.TestData, getData );
        }

        /// <summary>   Move to key. </summary>
        /// <remarks>
        /// Remark added by David, 2020-12-11 <para>
        /// Uses the <see cref="Fixture"/> <see cref="EnvironmentFixture"/> 
        /// and multi-Value <see cref="DatabaseFixture"/>.
        /// Moves to a specify key and validates key value and data value.
        /// 
        /// </para>
        /// <list type="number"><listheader>Values</listheader><item>
        /// 4: Test Data 4    </item><item>
        /// 19: Test Data 19a </item><item>
        /// </item> </remarks>
        [Fact]
        public void MoveToKey()
        {
            using var tx = this._Fixture.Env.BeginReadOnlyTransaction();
            using var cursor = Cursor.OpenCursor( this._Fixture.Db, tx );
            var keyBytes = BitConverter.GetBytes( 4 );

            // move to key = 4
            Assert.True( cursor.TryMoveToKey( keyBytes ) );
            Assert.True( cursor.TryGetCurrent( out KeyDataPair entry ) );

            // validate key value
            var ckey = BitConverter.ToInt32( entry.Key );
            Assert.Equal( 4, ckey );

            // validate data value
            var cdata = Encoding.UTF8.GetString( entry.Data );
            this._Output.WriteLine( $"{ckey}: {cdata}; expecting: {this._Fixture.TestData[4][0]}" );
            Assert.Equal( this._Fixture.TestData[4][0], cdata );

            // select the odd index with a gap of 12 from the first count (7) = 19
            int secondIndx = DatabaseFixture.FirstCount + DatabaseFixture.Gap;
            if ( secondIndx % 2 == 0 )
                secondIndx++;  // make sure it is odd

            // move to key
            keyBytes = BitConverter.GetBytes( secondIndx );
            Assert.True( cursor.TryMoveToKey( keyBytes ) );
            Assert.True( cursor.TryGetCurrent( out entry ) );

            // validate key value
            ckey = BitConverter.ToInt32( entry.Key );
            Assert.Equal( secondIndx, ckey );

            // validate key data.
            cdata = Encoding.UTF8.GetString( entry.Data );
            this._Output.WriteLine( $"{ckey}: {cdata}; expecting {this._Fixture.TestData[secondIndx][0]}" );
            Assert.Equal( this._Fixture.TestData[secondIndx][0], cdata );

        }

        /// <summary>   Move to key iterate multiple. </summary>
        /// <remarks>
        /// Remark added by David, 2020-12-11 <para>
        /// Uses the <see cref="Fixture"/> <see cref="EnvironmentFixture"/> 
        /// and multi-Value <see cref="DatabaseFixture"/>.
        /// Move to cursor 19 and iterate over all it duplicate values.
        /// </para>
        /// <list type="number"><listheader>Values</listheader><item>
        /// 19: Test Data 19a</item><item>
        /// 19: Test Data 19b</item><item>
        /// 19: Test Data 19c</item><item>
        /// 19: Test Data 19d</item><item>
        /// 19: Test Data 19e</item><item>
        /// 19: Test Data 19f</item><item>
        /// 19: Test Data 19g</item><item>
        /// 19: Test Data 19h</item><item>
        /// 19: Test Data 19i</item><item>
        /// 19: Test Data 19j</item><item>
        /// 19: Test Data 19k</item><item>
        /// 19: Test Data 19l</item><item>
        /// 19: Test Data 19m</item><item>
        /// 19: Test Data 19n</item><item>
        /// 19: Test Data 19o</item><item>
        /// 19: Test Data 19p</item><item>
        /// 19: Test Data 19q</item><item>
        /// 19: Test Data 19r</item><item>
        /// 19: Test Data 19s</item><item>
        /// </item> </remarks>
        [Fact]
        public void MoveToKeyIterateMultiple()
        {
            // move to key 19
            using var tx = this._Fixture.Env.BeginReadOnlyTransaction();
            using var cursor = Cursor.OpenCursor( this._Fixture.Db, tx );
            int secondIndx = DatabaseFixture.FirstCount + DatabaseFixture.Gap;
            if ( secondIndx % 2 == 0 )
                secondIndx++;  // make sure it is odd

            var keyBytes = BitConverter.GetBytes( secondIndx );
            Assert.True( cursor.TryMoveToKey( keyBytes ) );

            // iterate over all duplicate values of the current cursor.
            int indx = 0;
            foreach ( var value in cursor.ValuesForward )
            {
                var cdata = Encoding.UTF8.GetString( value );
                this._Output.WriteLine( $"{secondIndx}: {cdata}; expecting: {this._Fixture.TestData[secondIndx][indx]}" );
                Assert.Equal( this._Fixture.TestData[secondIndx][indx], cdata );
                indx++;
            }
        }

        /// <summary>   Iterate over key range 1. </summary>
        /// <remarks>
        /// Remark added by David, 2020-12-11 <para>
        /// Uses the <see cref="Fixture"/> <see cref="EnvironmentFixture"/> 
        /// and multi-Value <see cref="DatabaseFixture"/>.
        /// Iterates through key elements for from Key 5 to 19.
        /// Starts using <see cref="Cursor.TryGetAt(in ReadOnlySpan{byte}, out KeyDataPair)"/>
        /// </para>
        /// <list type="number"><listheader>Values</listheader><item>
        /// 5: Test Data 5a</item><item>
        /// 5: Test Data 5b</item><item>
        /// 5: Test Data 5c</item><item>
        /// 5: Test Data 5d</item><item>
        /// 5: Test Data 5e</item><item>
        /// 6: Test Data 6</item><item>
        /// 19: Test Data 19a</item><item>
        /// 19: Test Data 19b</item><item>
        /// 19: Test Data 19c</item><item>
        /// 19: Test Data 19d</item><item>
        /// 19: Test Data 19e</item><item>
        /// 19: Test Data 19f</item><item>
        /// 19: Test Data 19g</item><item>
        /// 19: Test Data 19h</item><item>
        /// 19: Test Data 19i</item><item>
        /// 19: Test Data 19j</item><item>
        /// 19: Test Data 19k</item><item>
        /// 19: Test Data 19l</item><item>
        /// 19: Test Data 19m</item><item>
        /// 19: Test Data 19n</item><item>
        /// 19: Test Data 19o</item><item>
        /// 19: Test Data 19p</item><item>
        /// 19: Test Data 19q</item><item>
        /// 19: Test Data 19r</item><item>
        /// 19: Test Data 19s
        /// </item> </remarks>
        [Fact]
        public void IterateOverKeyRange1()
        {

            // Set start key to 5 and end key to 19
            int startKey = DatabaseFixture.FirstCount - 2;
            int endKey = DatabaseFixture.FirstCount + DatabaseFixture.Gap;
            if ( endKey % 2 == 0 )
                endKey++;  // make sure it is odd

            using var tx = this._Fixture.Env.BeginReadOnlyTransaction();
            using var cursor = Cursor.OpenCursor( this._Fixture.Db, tx );
            var startKeyBytes = BitConverter.GetBytes( startKey );
            Assert.True( cursor.TryGetAt( startKeyBytes, out var firstEntry ) );
            var cdata = Encoding.UTF8.GetString( firstEntry.Data );

            // validate the data at the start key.
            this._Output.WriteLine( $"{startKey}: {cdata}; expecting: {this._Fixture.TestData[startKey][0]}" );
            Assert.Equal( this._Fixture.TestData[startKey][0], cdata );

            int dupIndex = 1;  // initialize to 1, as we already retrieved the entry at index 0
            int previousKey = startKey;
            var endKeyBytes = BitConverter.GetBytes( endKey );

            // iterate over records in sort order from the next position
            foreach ( var entry in cursor.ForwardFromNext )
            {
                if ( this._Fixture.Db.Compare( tx, entry.Key, endKeyBytes ) > 0 ) // end of range test
                    break;

                var ckey = BitConverter.ToInt32( entry.Key );
                // when the key changes then we re-start the duplicates index
                if ( ckey != previousKey )
                {
                    previousKey = ckey;
                    dupIndex = 0;
                }

                cdata = Encoding.UTF8.GetString( entry.Data );
                this._Output.WriteLine( $"{ckey}: {cdata}; expecting {this._Fixture.TestData[ckey][dupIndex]}" );
                Assert.Equal( this._Fixture.TestData[ckey][dupIndex], cdata );
                dupIndex++;
            }
        }

        /// <summary>   Iterate over key range 2. </summary>
        /// <remarks>
        /// Remark added by David, 2020-12-11 <para>
        /// Uses the <see cref="Fixture"/> <see cref="EnvironmentFixture"/> 
        /// and multi-Value <see cref="DatabaseFixture"/>.
        /// Iterates through key elements for from Key 5 to 19.
        /// Starts using <see cref="Cursor.TryMoveToKey(in ReadOnlySpan{byte})"/>
        /// </para>
        /// <list type="number"><listheader>Values</listheader><item>
        /// 5: Test Data 5a</item><item>
        /// 5: Test Data 5b</item><item>
        /// 5: Test Data 5c</item><item>
        /// 5: Test Data 5d</item><item>
        /// 5: Test Data 5e</item><item>
        /// 6: Test Data 6</item><item>
        /// 19: Test Data 19a</item><item>
        /// 19: Test Data 19b</item><item>
        /// 19: Test Data 19c</item><item>
        /// 19: Test Data 19d</item><item>
        /// 19: Test Data 19e</item><item>
        /// 19: Test Data 19f</item><item>
        /// 19: Test Data 19g</item><item>
        /// 19: Test Data 19h</item><item>
        /// 19: Test Data 19i</item><item>
        /// 19: Test Data 19j</item><item>
        /// 19: Test Data 19k</item><item>
        /// 19: Test Data 19l</item><item>
        /// 19: Test Data 19m</item><item>
        /// 19: Test Data 19n</item><item>
        /// 19: Test Data 19o</item><item>
        /// 19: Test Data 19p</item><item>
        /// 19: Test Data 19q</item><item>
        /// 19: Test Data 19r</item><item>
        /// 19: Test Data 19s</item><item>
        /// </item> </remarks>
        [Fact]
        public void IterateOverKeyRange2()
        {

            // Set start key to 5 and end key to 19
            int startKey = DatabaseFixture.FirstCount - 2;
            int endKey = DatabaseFixture.FirstCount + DatabaseFixture.Gap;
            if ( endKey % 2 == 0 )
                endKey++;  // make sure it is odd

            using var tx = this._Fixture.Env.BeginReadOnlyTransaction();
            using var cursor = Cursor.OpenCursor( this._Fixture.Db, tx );
            var startKeyBytes = BitConverter.GetBytes( startKey );
            Assert.True( cursor.TryMoveToKey( startKeyBytes ) );

            int dupIndex = 0;  
            int previousKey = startKey;
            var endKeyBytes = BitConverter.GetBytes( endKey );
            foreach ( var entry in cursor.ForwardFromCurrent )
            {
                if ( this._Fixture.Db.Compare( tx, entry.Key, endKeyBytes ) > 0 ) // end of range test
                    break;

                var ckey = BitConverter.ToInt32( entry.Key );
                // when the key changes then we re-start the duplicates index
                if ( ckey != previousKey )
                {
                    previousKey = ckey;
                    dupIndex = 0;
                }

                var cdata = Encoding.UTF8.GetString( entry.Data );
                this._Output.WriteLine( $"{ckey}: {cdata}; expecting {this._Fixture.TestData[ckey][dupIndex]}" );
                Assert.Equal( this._Fixture.TestData[ckey][dupIndex], cdata );
                dupIndex++;

            }
        }

        /// <summary>   Iterate over duplicates range. </summary>
        /// <remarks>
        /// Remark added by David, 2020-12-11 <para>
        /// Uses the <see cref="Fixture"/> <see cref="EnvironmentFixture"/> 
        /// and multi-Value <see cref="DatabaseFixture"/>.
        ///  Iterate over duplicate records of the current key between two values.
        /// </para>
        /// <list type="number"><listheader>Values</listheader><item>
        /// 19: Test Data 19c</item><item>
        /// 19: Test Data 19d</item><item>
        /// 19: Test Data 19e</item><item>
        /// </item> </remarks>
        [Fact]
        public void IterateOverDuplicatesRange()
        {
            // select odd key 19
            int key = DatabaseFixture.FirstCount + DatabaseFixture.Gap;
            if ( key % 2 == 0 )
                key++;  // make sure it is odd
            var keyBytes = BitConverter.GetBytes( key );

            // set start and end data values
            int dupIndex = 2;
            string startData = this._Fixture.TestData[key][dupIndex];
            var startDataBytes = Encoding.UTF8.GetBytes( startData );
            string endData = this._Fixture.TestData[key][DatabaseFixture.SecondCount - 1];
            var endDataBytes = Encoding.UTF8.GetBytes( endData );

            var startEntry = new KeyDataPair( keyBytes, startDataBytes );

            using var tx = this._Fixture.Env.BeginReadOnlyTransaction();
            using var cursor = Cursor.OpenCursor( this._Fixture.Db, tx );
            Assert.True( cursor.TryGetAt( startEntry, out var tmpEntry ) );

            // iterate over duplicate records of the current key.
            foreach ( var dataBytes in cursor.ValuesForwardFromCurrent )
            {
                // query if end of range
                if ( this._Fixture.Db.DupCompare( tx, dataBytes, endDataBytes ) > 0 )
                    break;

                var cdata = Encoding.UTF8.GetString( dataBytes );
                this._Output.WriteLine( $"{key}: {cdata}; expecting {this._Fixture.TestData[key][dupIndex]}" );
                Assert.Equal( this._Fixture.TestData[key][dupIndex], cdata );
                dupIndex++;
            }
        }

        /// <summary>   Move to data simple. </summary>
        /// <remarks>
        /// Remark added by David, 2020-12-11 <para>
        /// Uses the <see cref="Fixture"/> <see cref="EnvironmentFixture"/>
        /// and multi-Value <see cref="DatabaseFixture"/>. Moves cursor to the nearest data value. </para>
        /// </remarks>
        [Fact]
        public void MoveToDataSimple()
        {
            using var tx = this._Fixture.Env.BeginReadOnlyTransaction();
            using var cursor = Cursor.OpenCursor( this._Fixture.Db, tx );
            var keyData = new KeyDataPair( BitConverter.GetBytes( 4 ), Encoding.UTF8.GetBytes( "Test Data" ) );
            Assert.True( cursor.TryGetNearest( keyData, out KeyDataPair entry ) );
        }

        /// <summary>   Move to data. </summary>
        /// <remarks>
        /// Remark added by David, 2020-12-11 <para>
        /// Uses the <see cref="Fixture"/> <see cref="EnvironmentFixture"/> 
        /// and multi-Value <see cref="DatabaseFixture"/>.
        /// Uses <see cref="Cursor.TryGetAt(in ReadOnlySpan{byte}, out KeyDataPair)"/> 
        /// and <see cref="Cursor.TryGetNearest(in ReadOnlySpan{byte}, out KeyDataPair)"/> to move to the key value pair.
        /// </para>
        /// <list type="number"><listheader>Values</listheader><item>
        /// 4: Test Data 4 </item><item>
        /// 19: Test Data 19c </item><item>
        /// </item> </remarks>
        [Fact]
        public void MoveToData()
        {

            var buffer = new byte[1024];
            using var tx = this._Fixture.Env.BeginReadOnlyTransaction();
            using var cursor = Cursor.OpenCursor( this._Fixture.Db, tx );
            var keyBytes = BitConverter.GetBytes( 4 );
            var dataString = this._Fixture.TestData[4][0];
            int byteCount = Encoding.UTF8.GetBytes( dataString, 0, dataString.Length, buffer, 0 );
            var keyData = new KeyDataPair( keyBytes, new ReadOnlySpan<byte>( buffer, 0, byteCount ) );
            Assert.True( cursor.TryGetAt( keyData, out KeyDataPair entry ) );

            var ckey = BitConverter.ToInt32( entry.Key );
            Assert.Equal( 4, ckey );
            var cdata = Encoding.UTF8.GetString( entry.Data );
            this._Output.WriteLine( $"{ckey}: {cdata}; expecting {this._Fixture.TestData[4][0]}" );
            Assert.Equal( this._Fixture.TestData[4][0], cdata );

            int secondIndx = DatabaseFixture.FirstCount + DatabaseFixture.Gap;
            if ( secondIndx % 2 == 0 )
                secondIndx++;  // make sure it is odd

            keyBytes = BitConverter.GetBytes( secondIndx );
            dataString = $"Test Data {secondIndx}bty"; // this is between the second and third duplicate (b, c)
            byteCount = Encoding.UTF8.GetBytes( dataString, 0, dataString.Length, buffer, 0 );
            keyData = new KeyDataPair( keyBytes, new ReadOnlySpan<byte>( buffer, 0, byteCount ) );
            Assert.True( cursor.TryGetNearest( keyData, out entry ) );

            ckey = BitConverter.ToInt32( entry.Key );
            Assert.Equal( secondIndx, ckey );
            cdata = Encoding.UTF8.GetString( entry.Data );
            this._Output.WriteLine( $"{ckey}: {cdata}; expecting {this._Fixture.TestData[secondIndx][2]}" );
            Assert.Equal( this._Fixture.TestData[secondIndx][2], cdata );  // we should be positioned at the third duplicate
        }

        /// <summary>   Iterate by key from key: Iterates over keys in sort order from next position on.. </summary>
        /// <remarks>
        /// Remark added by David, 2020-12-11 <para>
        /// Uses the <see cref="Fixture"/> <see cref="EnvironmentFixture"/> 
        /// and multi-Value <see cref="DatabaseFixture"/>.
        /// Iterates over keys in sort order from next position on.
        /// </para>
        /// <list type="number"><listheader>Values</listheader><item>
        /// 4: Test Data 4</item><item>
        /// 5: Test Data 5a</item><item>
        /// 6: Test Data 6</item><item>
        /// 19: Test Data 19a</item><item>
        /// 20: Test Data 20</item><item>
        /// 21: Test Data 21a</item><item>
        /// 22: Test Data 22</item><item>
        /// 23: Test Data 23a</item><item>
        /// =================</item><item>
        /// 19: Test Data 19a</item><item>
        /// 20: Test Data 20</item><item>
        /// 21: Test Data 21a</item><item>
        /// 22: Test Data 22</item><item>
        /// 23: Test Data 23a</item><item>
        /// </item> </remarks>
        [Fact]
        public void IterateByKeyFromKey()
        {
            var byKeyList = new List<(int, string)>();
            var fromNearestList = new List<(int, string)>();

            using ( var tx = this._Fixture.Env.BeginReadOnlyTransaction() )
            {
                using var cursor = Cursor.OpenCursor( this._Fixture.Db, tx );
                int key = DatabaseFixture.FirstCount - 3;
                var keyBytes = BitConverter.GetBytes( key );

                Assert.True( cursor.TryGetAt( keyBytes, out KeyDataPair entry ) );
                var ckey = BitConverter.ToInt32( entry.Key );
                var cdata = Encoding.UTF8.GetString( entry.Data );
                byKeyList.Add( (ckey, cdata) );
                this._Output.WriteLine( $"Iterate from {ckey}:" );
                this._Output.WriteLine( $"{ckey}: {cdata}" );

                foreach ( var fwEntry in cursor.ForwardFromNextByKey )
                {
                    ckey = BitConverter.ToInt32( fwEntry.Key );
                    cdata = Encoding.UTF8.GetString( fwEntry.Data );
                    byKeyList.Add( (ckey, cdata) );
                    this._Output.WriteLine( $"{ckey}: {cdata}" );
                }

                key = DatabaseFixture.FirstCount + 1;
                keyBytes = BitConverter.GetBytes( key );

                this._Output.WriteLine( $"Iterate from {ckey}:" );

                Assert.True( cursor.TryGetNearest( keyBytes, out entry ) );
                ckey = BitConverter.ToInt32( entry.Key );
                cdata = Encoding.UTF8.GetString( entry.Data );
                fromNearestList.Add( (ckey, cdata) );
                this._Output.WriteLine( $"{ckey}: {cdata}" );

                foreach ( var fwEntry in cursor.ForwardFromNextByKey )
                {
                    ckey = BitConverter.ToInt32( fwEntry.Key );
                    cdata = Encoding.UTF8.GetString( fwEntry.Data );
                    fromNearestList.Add( (ckey, cdata) );
                    this._Output.WriteLine( $"{ckey}: {cdata}" );
                }
            }

            int startKey = (DatabaseFixture.FirstCount - 3);
            var orderedByKeys = this._Fixture.TestData.Keys.Where( k => k >= startKey ).OrderBy( k => k ).ToList();
            Assert.Equal( orderedByKeys.Count, byKeyList.Count );
            for ( int indx = 0; indx < orderedByKeys.Count; indx++ )
            {
                var compKey = orderedByKeys[indx];
                AssertX.Equal( compKey, byKeyList[indx].Item1, $"Key from the {indx} item of the built nearest list should equal Compare key {compKey}" );
                Assert.Equal( this._Fixture.TestData[compKey][0], byKeyList[indx].Item2 );
            }

            startKey = (DatabaseFixture.FirstCount + DatabaseFixture.Gap);
            var fromNearestKeys = this._Fixture.TestData.Keys.Where( k => k >= startKey ).OrderBy( k => k ).ToList();
            for ( int indx = 0; indx < fromNearestKeys.Count; indx++ )
            {
                var compKey = fromNearestKeys[indx];
                AssertX.Equal( compKey, fromNearestList[indx].Item1 , $"Key from the {indx} item of the built nearest list should equal Compare key {compKey}" );
                AssertX.Equal( this._Fixture.TestData[compKey][0], fromNearestList[indx].Item2,
                               $"Value from the {indx} item of the built nearest list should equal database data value" );
            }
        }

        /// <summary>   Iterate from entry. </summary>
        /// <remarks>
        /// Remark added by David, 2020-12-11 <para>
        /// Uses the <see cref="Fixture"/> <see cref="EnvironmentFixture"/> 
        /// and multi-Value <see cref="DatabaseFixture"/>.
        /// Iterates over records in sort order from next position on.
        /// </para>
        /// <list type="number"><listheader>Values</listheader><item>
        /// 3: Test Data 3b</item><item>
        /// 3: Test Data 3c</item><item>
        /// 4: Test Data 4</item><item>
        /// 5: Test Data 5a  </item><item>
        /// ....</item><item>
        /// 23: Test Data 23u</item><item>
        /// 23: Test Data 23v</item><item>
        /// 23: Test Data 23w</item><item>
        /// =================</item><item>
        /// 19: Test Data 19a</item><item>
        /// 19: Test Data 19b</item><item>
        /// 19: Test Data 19c</item><item>
        /// ....</item><item>
        /// 23: Test Data 23t</item><item>
        /// 23: Test Data 23u</item><item>
        /// 23: Test Data 23v</item><item>
        /// 23: Test Data 23w</item><item>
        /// </item> </remarks>
        [Fact]
        public void IterateFromEntry()
        {
            var entryList = new List<(int, string)>();
            var fromNearestList = new List<(int, string)>();

            using ( var tx = this._Fixture.Env.BeginReadOnlyTransaction() )
            {
                using var cursor = Cursor.OpenCursor( this._Fixture.Db, tx );
                int key = DatabaseFixture.FirstCount - 4;
                var keyBytes = BitConverter.GetBytes( key );
                var data = Encoding.UTF8.GetBytes( this._Fixture.TestData[key][1] );

                this._Output.WriteLine( $"Iterate from {key}" );

                Assert.True( cursor.TryGetAt( new KeyDataPair( keyBytes, data ), out KeyDataPair entry ) );
                var ckey = BitConverter.ToInt32( entry.Key );
                var cdata = Encoding.UTF8.GetString( entry.Data );
                entryList.Add( (ckey, cdata) );
                this._Output.WriteLine( $"{ckey}: {cdata}" );

                foreach ( var fwEntry in cursor.ForwardFromNext )
                {
                    ckey = BitConverter.ToInt32( fwEntry.Key );
                    cdata = Encoding.UTF8.GetString( fwEntry.Data );
                    entryList.Add( (ckey, cdata) );
                    this._Output.WriteLine( $"{ckey}: {cdata}" );
                }

                this._Output.WriteLine( $"Iterate from next key of nearest to {key}" );

                key = DatabaseFixture.FirstCount + 1;
                keyBytes = BitConverter.GetBytes( key );

                Assert.True( cursor.TryGetNearest( keyBytes, out entry ) );
                ckey = BitConverter.ToInt32( entry.Key );
                cdata = Encoding.UTF8.GetString( entry.Data );
                fromNearestList.Add( (ckey, cdata) );
                this._Output.WriteLine( $"{ckey}: {cdata}" );

                // if we used ForwardFromNearest(in KeyDataPair) then we would need to start at an existing key
                foreach ( var fwEntry in cursor.ForwardFromNext )
                {
                    ckey = BitConverter.ToInt32( fwEntry.Key );
                    cdata = Encoding.UTF8.GetString( fwEntry.Data );
                    fromNearestList.Add( (ckey, cdata) );
                    this._Output.WriteLine( $"{ckey}: {cdata}" );
                }
            }

            int startKey = (DatabaseFixture.FirstCount - 4);
            var orderedKeys = this._Fixture.TestData.Keys.Where( k => k >= startKey ).OrderBy( k => k ).ToList();
            var expectedList = new List<(int, string)>();
            foreach ( var key in orderedKeys )
            {
                var dataList = this._Fixture.TestData[key];
                for ( int indx = 0; indx < dataList.Count; indx++ )
                {
                    // we start at the second value of the first key
                    if ( key == startKey && indx == 0 )
                        continue;
                    expectedList.Add( (key, dataList[indx]) );
                }
            }
            Assert.Equal( expectedList, entryList );

            startKey = (DatabaseFixture.FirstCount + DatabaseFixture.Gap);
            var fromNearestKeys = this._Fixture.TestData.Keys.Where( k => k >= startKey ).OrderBy( k => k ).ToList();
            expectedList.Clear();
            foreach ( var key in fromNearestKeys )
            {
                // we are not skipping any values
                foreach ( var data in this._Fixture.TestData[key] )
                    expectedList.Add( (key, data) );
            }
            Assert.Equal( expectedList, fromNearestList );
        }

        /// <summary>   Iterate from entry reverse. </summary>
        /// <remarks>
        /// Remark added by David, 2020-12-11 <para>
        /// Uses the <see cref="Fixture"/> <see cref="EnvironmentFixture"/> 
        /// and multi-Value <see cref="DatabaseFixture"/>.
        /// Iterates over records in reverse sort order from the previous position on.
        /// </para>
        /// <list type="number"><listheader>Values</listheader><item>
        /// Iterate from 3:
        /// 3: Test Data 3c
        /// 3: Test Data 3b
        /// 3: Test Data 3a
        /// 2: Test Data 2
        /// 1: Test Data 1a
        /// 0: Test Data 0
        /// Iterate from 21:
        /// 21: Test Data 21f
        /// 21: Test Data 21e
        /// 21: Test Data 21d
        /// 21: Test Data 21c
        /// 21: Test Data 21b
        /// ....
        /// 19: Test Data 19b
        /// 19: Test Data 19a
        /// 6: Test Data 6
        /// 5: Test Data 5e
        /// 5: Test Data 5d  
        /// ...
        /// 1: Test Data 1a
        /// 0: Test Data 0
        /// </item><item>
        /// </item><item>
        /// </item><item>
        /// </item><item>
        /// </item><item>
        /// </item><item>
        /// </item> </remarks>
        [Fact]
        public void IterateFromEntryReverse()
        {
            var entryList = new List<(int, string)>();
            var fromNearestList = new List<(int, string)>();

            using ( var tx = this._Fixture.Env.BeginReadOnlyTransaction() )
            {
                using var cursor = Cursor.OpenCursor( this._Fixture.Db, tx );
                int key = (DatabaseFixture.FirstCount - 4);
                var keyBytes = BitConverter.GetBytes( key );
                var data = Encoding.UTF8.GetBytes( this._Fixture.TestData[key][2] );

                Assert.True( cursor.TryGetAt( new KeyDataPair( keyBytes, data ), out KeyDataPair entry ) );
                var ckey = BitConverter.ToInt32( entry.Key );
                var cdata = Encoding.UTF8.GetString( entry.Data );
                entryList.Add( (ckey, cdata) );
                this._Output.WriteLine( $"Iterate from {ckey}:");
                this._Output.WriteLine( $"{ckey}: {cdata}" );

                foreach ( var rvEntry in cursor.ReverseFromPrevious )
                {
                    ckey = BitConverter.ToInt32( rvEntry.Key );
                    cdata = Encoding.UTF8.GetString( rvEntry.Data );
                    entryList.Add( (ckey, cdata) );
                    this._Output.WriteLine( $"{ckey}: {cdata}" );
                }

                key = DatabaseFixture.FirstCount + DatabaseFixture.Gap + 1;
                if ( key % 2 == 0 )
                    key++;  // make sure it is odd

                this._Output.WriteLine( $"Iterate from {key}:" );
                keyBytes = BitConverter.GetBytes( key );
                var dataStr = this._Fixture.TestData[key][5] + "zz";
                data = Encoding.UTF8.GetBytes( dataStr );

                Assert.True( cursor.TryGetNearest( new KeyDataPair( keyBytes, data ), out entry ) );
                // this got us the next record *after* data, but since we want to start with the one before,
                // we ignore the current entry and just use the loop, as it will start with the record we want

                // if we used ForwardFromNearestEntry then at least the key would have to match
                foreach ( var rvEntry in cursor.ReverseFromPrevious )
                {
                    ckey = BitConverter.ToInt32( rvEntry.Key );
                    cdata = Encoding.UTF8.GetString( rvEntry.Data );
                    fromNearestList.Add( (ckey, cdata) );
                    this._Output.WriteLine( $"{ckey}: {cdata}" );
                }
            }

            int startKey = (DatabaseFixture.FirstCount - 4);
            var orderedKeys = this._Fixture.TestData.Keys.Where( k => k <= startKey ).OrderByDescending( k => k ).ToList();
            var expectedList = new List<(int, string)>();
            foreach ( var key in orderedKeys )
            {
                var dataList = this._Fixture.TestData[key];
                for ( int indx = dataList.Count - 1; indx >= 0; indx-- )
                {
                    // we ignore some values of the highest key
                    if ( key == startKey && indx > 2 )
                        continue;
                    expectedList.Add( (key, dataList[indx]) );
                }
            }
            Assert.Equal( expectedList, entryList );

            startKey = (DatabaseFixture.FirstCount + DatabaseFixture.Gap + 1);
            if ( startKey % 2 == 0 )
                startKey++;  // make sure it is odd
            var fromNearestKeys = this._Fixture.TestData.Keys.Where( k => k <= startKey ).OrderByDescending( k => k ).ToList();
            expectedList.Clear();
            foreach ( var key in fromNearestKeys )
            {
                var dataList = this._Fixture.TestData[key];
                for ( int indx = dataList.Count - 1; indx >= 0; indx-- )
                {
                    // we ignore some values of the highest key
                    if ( key == startKey && indx > 5 )
                        continue;
                    expectedList.Add( (key, dataList[indx]) );
                }
            }
            Assert.Equal( expectedList, fromNearestList );
        }
    }
}
