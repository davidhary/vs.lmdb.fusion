using System;
using System.Collections.Generic;
using System.Text;

using Xunit;

namespace isr.Lmdb.Fusion.Tests
{
    /// <summary>   A fixed size database fixture. </summary>
    /// <remarks>
    /// Remark added by David, 2020-12-11. A single test context to be shared among tests in several
    /// test classes, and cleaned up after all the tests in the test classes have finished.
    /// </remarks>
    public class FixedSizeDatabaseFixture : EnvironmentFixture
    {

        /// <summary>   Name of the database. </summary>
        public const string DatabaseName = "FixedSizeDatabase";

        /// <summary>   Int key compare. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="x">    A ReadOnlySpan&lt;byte&gt; to process. </param>
        /// <param name="y">    A ReadOnlySpan&lt;byte&gt; to process. </param>
        /// <returns>   An int. </returns>
        public static int IntKeyCompare( in ReadOnlySpan<byte> x, in ReadOnlySpan<byte> y )
        {
            var xInt = BitConverter.ToInt32( x.ToArray(), 0 );
            var yInt = BitConverter.ToInt32( y.ToArray(), 0 );
            return Comparer<int>.Default.Compare( xInt, yInt );
        }

        /// <summary>   Unique identifier data compare. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="x">    A ReadOnlySpan&lt;byte&gt; to process. </param>
        /// <param name="y">    A ReadOnlySpan&lt;byte&gt; to process. </param>
        /// <returns>   An int. </returns>
        public static int GuidDataCompare( in ReadOnlySpan<byte> x, in ReadOnlySpan<byte> y )
        {
            var xStr = Encoding.UTF8.GetString( x.ToArray() );
            var yStr = Encoding.UTF8.GetString( y.ToArray() );
            return StringComparer.Ordinal.Compare( xStr, yStr );
        }

        /// <summary>   Default constructor. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        public FixedSizeDatabaseFixture()
        {
            int recordSize = Guid.NewGuid().ToString().Length * 2;

            using ( var tx = this.Env.BeginOpenDatabaseTransaction( TransactionBeginOptions.None ) )
            {
                var config = new DatabaseConfiguration( DatabaseOpenOptions.Create | DatabaseOpenOptions.DuplicatesFixed, new Interop.LmdbValue(),
                                                       FixedSizeDatabaseFixture.IntKeyCompare,
                                                       FixedSizeDatabaseFixture.GuidDataCompare ) { FixedDataSize = recordSize, };
                this.Db = tx.OpenFixedMultiValueDatabase( DatabaseName, config );
                tx.Commit();
            }

            var stats = this.Env.GetStats();
            var pgSize = stats.PageSize;

            // Lets fill 100 pages with 20 GUIDS per key

            this.TestData = new Dictionary<int, List<string>>();

            int maxSize = ( int ) (100 * pgSize);
            int totalSize = 0;

            int key = 0;
            while ( totalSize < maxSize )
            {
                var data = new List<string>( 20 );
                for ( int gi = 0; gi < 20; gi++ )
                {
                    var guid = Guid.NewGuid().ToString();
                    data.Add( guid );
                    totalSize += recordSize;
                }
                this.TestData[key] = data;
                key++;
            }
        }

        /// <summary>   Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        public Dictionary<int, List<string>> TestData { get; }

        /// <summary>   Gets the fixed size database. </summary>
        /// <value> The database. </value>
        public Database Db { get; }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="disposing">    True to release both managed and unmanaged resources; false to
        ///                             release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                using var tx = this.Env.BeginTransaction( TransactionBeginOptions.None );
                this.Db.DropThrow( tx );
                tx.Commit();
            }
            base.Dispose( disposing );
        }
    }

    /// <summary>   Defines the <see cref="FixedSizeDatabaseFixture"/> Collections. </summary>
    /// <remarks>   Remark added by David, 2020-12-11. </remarks>
    [CollectionDefinition( nameof( FixedSizeDatabaseCollection ) )]
    public class FixedSizeDatabaseCollection : ICollectionFixture<FixedSizeDatabaseFixture>
    {
        // This class has no code, and is never created. Its purpose is simply to be
        // the place to apply [CollectionDefinition] and all the ICollectionFixture<> interfaces. 
    }
}
