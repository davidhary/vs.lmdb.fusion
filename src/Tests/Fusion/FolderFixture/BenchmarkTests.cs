using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

using isr.Lmdb.Fusion.Interop;

using Xunit;
using Xunit.Abstractions;

namespace isr.Lmdb.Fusion.Tests
{

    /// <summary>   A benchmark tests. </summary>
    /// <remarks>   David, 2021-01-21. </remarks>
    [Collection( nameof( FolderCollection ) )]
    public class BenchmarkTests
    {

        /// <summary>   The output. </summary>
        private readonly ITestOutputHelper _Output;

        /// <summary>   Gets the folder fixture. </summary>
        /// <value> The folder fixture. </value>
        private FolderFixture FolderFixture { get; }

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="folderFixture">    The folder fixture. </param>
        /// <param name="output">           The output. </param>
        public BenchmarkTests( FolderFixture folderFixture, ITestOutputHelper output )
        {
            this.FolderFixture = folderFixture;
            this._Output = output;
        }

        /// <summary>   (Unit Test Method) Benchmark Cursor Get Integer Key Value pairs. </summary>
        /// <remarks>   David, 2020-12-16.  <para>
        /// LMDB 0.9.70: (December 19, 2015) </para><para>
        /// Case                 |    Count |    MOPS | Elapsed, us |   GC0 |   GC1 |   GC2 | Memory, MB | </para><para>
        /// Reuse cursor get     |    1,000 |   1.379 |         725 |   0.0 |   0.0 |   0.0 |      0.000 | </para><para>
        /// Reuse cursor get     |   10,000 |   6.218 |       1,608 |   0.0 |   0.0 |   0.0 |      1.766 | </para><para>
        /// Reuse cursor get     |   10,000 |   9.141 |       1,094 |   0.0 |   0.0 |   0.0 |      0.109 | </para><para>
        /// Last test ran with parallelization disabled on the <see cref="FolderFixture"/> 
        /// </para> </remarks>
        [Fact]
        public void CursorGetIntegerKeyValuePairs()
        {
            string testItemMessage;
            string testItemName;
            this._Output.WriteLine( LmdbVersionInfo.LmdbVersionMessage );
            var path = this.FolderFixture.CreateDirectoryForTest();
            using var env = LmdbEnvironment.OpenNew( path );
            testItemName = $"{nameof( LmdbEnvironment )}.{nameof( LmdbEnvironment.IsOpen )}";
            testItemMessage = $"{nameof( LmdbEnvironment )} should be open ({testItemName}) after {nameof( LmdbEnvironment.Open )}";
            Assert.True( env.IsOpen, testItemMessage );

            using var db = env.OpenDatabase( nameof( BenchmarkTests.CursorGetIntegerKeyValuePairs ), new DatabaseConfiguration( DatabaseOpenOptions.Create ) );

            var values = new byte[] { 1, 2, 3, 4 };

            {
                var key = new LmdbBuffer( values );
                var value = new LmdbBuffer( values );
                using var txn = env.BeginTransaction( TransactionBeginOptions.None );
                try
                {
                    using var cursor = Cursor.OpenCursor( db, txn );
                    Assert.True( 0 == cursor.Put( ref key, ref value, CursorPutOptions.None ), $"Cursor put should succeed" );
                    txn.Commit();
                }
                catch
                {
                    txn.Abort();
                    throw;
                }
            }

            var count = 10000;

            {
                using var txn = env.BeginReadOnlyTransaction( TransactionBeginOptions.None );
                var key = new LmdbBuffer( values );
                var value = new LmdbBuffer( values );
                LmdbBuffer value2 = default;
                Benchmark.Stat stat = Benchmark.Run( this._Output, "Reuse cursor get", count );
                using var cursor = Cursor.OpenCursor( db, txn );
                for ( int i = 0; i < count; i++ )
                {
                    Assert.True( 0 == cursor.Get( ref key, ref value2, CursorGetOption.SetKey ), $"Cursor get should succeed" );
                }
                cursor.Dispose();
                txn.Dispose();
                stat.Dispose();
                Assert.True( value2.Span != null, $"value2.span should not be null" );
                Assert.True( values.Length == value2.Span.Length, $"value2.span length should match" );
                for ( int i = 0; i < values.Length; i++ )
                {
                    Assert.True( values[i] == value2.Span[i], $"{i}: values {values[i]} value2.span {value2.Span[i]} should match" );
                }
                Assert.True( values.SequenceEqual( value2.Span.ToArray() ), $"Cursor Get values should equal cursor put values" );

            }

        }

        /// <summary>
        /// (Unit Test Method) benchmark cursor put integer key value pairs.
        /// </summary>
        /// <remarks>   David, 2020-12-16. <para>
        /// This test must be ran standalone as it breaks other tests. </para><para>
        ///  Case                 |    Count |    MOPS | Elapsed, us |   GC0 |   GC1 |   GC2 | Memory, MB | </para><para>
        ///  Cursor put           |    1,000 |   0.138 |       7,260 |   0.0 |   0.0 |   0.0 |      0.539 | </para><para>
        ///  Cursor put           |    1,000 |   0.222 |       4,503 |   0.0 |   0.0 |   0.0 |      1.022 | </para><para>
        /// Last test ran with parallelization disabled on the <see cref="FolderFixture"/>  </para><para>
        /// This test often breaks other tests. Turning off parallelization seems to have solved this issue.
        /// </para> </remarks>
        [Fact]
        [Trait( "Benchmark", "Standalone" )]
        public void CursorPutIntegerKeyValuePairs()
        {
            string testItemMessage;
            string testItemName;
            this._Output.WriteLine( LmdbVersionInfo.LmdbVersionMessage );
            var path = this.FolderFixture.CreateDirectoryForTest();
            using var env = LmdbEnvironment.OpenNew( path, EnvironmentOpenOptions.WriteMap | EnvironmentOpenOptions.NoSync );
            testItemName = $"{nameof( LmdbEnvironment )}.{nameof( LmdbEnvironment.IsOpen )}";
            testItemMessage = $"{nameof( LmdbEnvironment )} should be open ({testItemName}) after {nameof( LmdbEnvironment.Open )}";
            Assert.True( env.IsOpen, testItemMessage );

            using var db = env.OpenDatabase( nameof( BenchmarkTests.CursorPutIntegerKeyValuePairs ), new DatabaseConfiguration( DatabaseOpenOptions.Create ) );

            var values = new byte[] { 1, 2, 3, 4 };

            var count = 1000;

            using Benchmark.Stat stat = Benchmark.Run( this._Output, "Cursor put", count );
            for ( var i = 0; i < count; i++ )
            {
                var key = new LmdbBuffer( values );
                var value = new LmdbBuffer( values );
                using var txn = env.BeginTransaction( TransactionBeginOptions.None );
                try
                {
                    using var cursor = Cursor.OpenCursor( db, txn );
                    Assert.True( 0 == cursor.Put( ref key, ref value, CursorPutOptions.None ), $"Cursor put should succeed" );
                    cursor.Dispose();
                    txn.Commit();
                }
                catch
                {
                    txn.Abort();
                    throw;
                }
                txn.Dispose();
            }
            stat.Dispose();

            {
                using var txn = env.BeginReadOnlyTransaction( TransactionBeginOptions.None );
                var key = new LmdbBuffer( values );
                var value = new LmdbBuffer( values );
                LmdbBuffer value2 = default;
                using var cursor = Cursor.OpenCursor( db, txn );
                Assert.True( 0 == cursor.Get( ref key, ref value2, CursorGetOption.SetKey ) );
                cursor.Dispose();
                txn.Dispose();
                Assert.True( values.SequenceEqual( value2.Span.ToArray() ) );
            }

            db.Dispose();
            env.Dispose();

        }

        /// <summary>   (Unit Test Method) Benchmark database put integer key value pairs. </summary>
        /// <remarks>
        /// David, 2020-12-16. <para>
        /// Case                 |    Count |    MOPS | Elapsed, us |   GC0 |   GC1 |   GC2 | Memory, MB | </para><para>
        /// DB put               |    1,000 |   0.281 |       3,556 |   0.0 |   0.0 |   0.0 |      0.000 | </para><para>
        /// DB put               |    1,000 |   0.304 |       3,293 |   0.0 |   0.0 |   0.0 |      0.039 | </para><para>
        /// Last test ran with parallelization disabled on the <see cref="FolderFixture"/>  </para><para>
        /// </para>
        /// </remarks>
        [Fact]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public void DbPutIntegerKeyValuePairs()
        {
            string testItemMessage;
            string testItemName;
            this._Output.WriteLine( LmdbVersionInfo.LmdbVersionMessage );
            var path = this.FolderFixture.CreateDirectoryForTest();
            using var env = LmdbEnvironment.OpenNew( path, 100 * 1024 * 1024, EnvironmentOpenOptions.WriteMap | EnvironmentOpenOptions.NoSync );
            testItemName = $"{nameof( LmdbEnvironment )}.{nameof( LmdbEnvironment.IsOpen )}";
            testItemMessage = $"{nameof( LmdbEnvironment )} should be open ({testItemName}) after {nameof( LmdbEnvironment.Open )}";
            Assert.True( env.IsOpen, testItemMessage );

            using var db = env.OpenDatabase( nameof( BenchmarkTests.DbPutIntegerKeyValuePairs ),
                                             new DatabaseConfiguration( DatabaseOpenOptions.Create | DatabaseOpenOptions.IntegerDuplicates ) );

            var valueHolder = new int[1];
            var mem = new Memory<int>( valueHolder );
            var handle = mem.Pin();
            var count = 1_000;

            using ( Benchmark.Run( this._Output, "DB put", count ) )
            {
                for ( var i = 1; i < count; i++ )
                {
                    try
                    {
                        valueHolder[0] = i;
                        _ = db.Put( 0, i, TransactionPutOptions.AppendDuplicateData );
                    }
                    catch ( Exception e )
                    {
                        this._Output.WriteLine( e.ToString() );
                    }
                }
            }
            handle.Dispose();
        }

        /// <summary>   (Unit Test Method) Benchmark database put find long key value pairs. </summary>
        /// <remarks>
        /// David, 2020-12-16. <para>
        /// LMDB 0.9.70: (December 19, 2015)
        /// Case                 |    Count |    MOPS | Elapsed, us |   GC0 |   GC1 |   GC2 | Memory, MB | </para><para>
        /// Long Write+Find      |      100 |   0.020 |       4,882 |   0.0 |   0.0 |   0.0 |      0.039 | </para><para>
        /// Long Write+Find      |      100 |   0.015 |       6,739 |   0.0 |   0.0 |   0.0 |      0.047 | </para><para>
        /// Long Write+Find      |      100 |   0.013 |       7,482 |   0.0 |   0.0 |   0.0 |      0.216 | </para><para>
        /// Last test ran with parallelization disabled on the <see cref="FolderFixture"/>  </para><para>
        /// </para>
        /// </remarks>
        /// <exception cref="XunitException">   Thrown when an Xunit error condition occurs. </exception>
        [Fact]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public void DbPutFindLongKeyValuePairs()
        {
            string testItemMessage;
            string testItemName;
            this._Output.WriteLine( LmdbVersionInfo.LmdbVersionMessage );
            var path = this.FolderFixture.CreateDirectoryForTest();
            using var env = LmdbEnvironment.OpenNew( path, 100 * 1024 * 1024, EnvironmentOpenOptions.WriteMap | EnvironmentOpenOptions.NoSync );
            testItemName = $"{nameof( LmdbEnvironment )}.{nameof( LmdbEnvironment.IsOpen )}";
            testItemMessage = $"{nameof( LmdbEnvironment )} should be open ({testItemName}) after {nameof( LmdbEnvironment.Open )}";
            Assert.True( env.IsOpen, testItemMessage );

            // must set the Fixed Data Size so as to open a fixed data cursor.
            using var db = env.OpenDatabase( nameof( BenchmarkTests.DbPutFindLongKeyValuePairs ),
                                             new DatabaseConfiguration( DatabaseOpenOptions.Create | DatabaseOpenOptions.IntegerDuplicates ) { FixedDataSize = sizeof( ulong ) } );

            ulong count = 100;
            using ( Benchmark.Run( this._Output, "Long Write+Find", ( long ) count ) )
            {
                for ( ulong i = 1; i < count; i++ )
                {
                    var key = 0;
                    var value = i;
                    try
                    {
                        _ = db.Put( 0, i, TransactionPutOptions.AppendDuplicateData );

                        using var txn = env.BeginReadOnlyTransaction();
                        if ( !db.TryFindDup( txn, Lookup.EQ, ref key, ref value ) )
                        {
                            throw new Xunit.Sdk.XunitException( "!db.TryFindDup( txn, Lookup.EQ, ref key, ref value )" );
                        }

                        if ( value != i )
                        {
                            throw new Xunit.Sdk.XunitException( $"value {value} != i {i}" );
                        }
                    }
                    catch ( Exception e )
                    {
                        Console.WriteLine( e.ToString() );
                    }
                }
            }
        }

        /// <summary>   (Unit Test Method) Benchmark dual thread database put key long value pairs. </summary>
        /// <remarks>
        /// David, 2021-01-04.  <para>
        /// Case                 |    Count |    MOPS | Elapsed, us |   GC0 |   GC1 |   GC2 | Memory, MB |  </para><para>
        /// Write 2              |    1,000 |   0.026 |      37,817 |   0.0 |   0.0 |   0.0 |      0.000 |  </para><para>
        /// Write 1              |    1,000 |   0.026 |      38,140 |   0.0 |   0.0 |   0.0 |      0.000 |  </para><para>
        /// Write 1              |    1,000 |   0.020 |      51,248 |   0.0 |   0.0 |   0.0 |      0.000 |  </para><para>
        /// Write 2              |    1,000 |   0.020 |      51,132 |   0.0 |   0.0 |   0.0 |      0.000 |  </para><para>
        /// Last test ran with parallelization disabled on the <see cref="FolderFixture"/>  </para><para>
        ///  </para></remarks>
        [Fact]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public void DualThreadDbPutKeyLongValuePairs()
        {

            string testItemMessage;
            string testItemName;
            this._Output.WriteLine( LmdbVersionInfo.LmdbVersionMessage );
            var path = this.FolderFixture.CreateDirectoryForTest();
            using var env = LmdbEnvironment.OpenNew( path, 100 * 1024 * 1024, EnvironmentOpenOptions.WriteMap | EnvironmentOpenOptions.NoSync );
            testItemName = $"{nameof( LmdbEnvironment )}.{nameof( LmdbEnvironment.IsOpen )}";
            testItemMessage = $"{nameof( LmdbEnvironment )} should be open ({testItemName}) after {nameof( LmdbEnvironment.Open )}";
            Assert.True( env.IsOpen, testItemMessage );

            using var db = env.OpenDatabase( nameof( BenchmarkTests.DualThreadDbPutKeyLongValuePairs ),
                                             new DatabaseConfiguration( DatabaseOpenOptions.Create | DatabaseOpenOptions.IntegerDuplicates ) );

            var keyValue = 0;
            var dataValue = 0L;
            var count = 1_000;

            var t1 = Task.Run( () => {
                using ( Benchmark.Run( this._Output, "Write 1", count ) )
                {
                    for ( var i = 1; i < count; i++ )
                    {
                        try
                        {
                            _ = db.Put( keyValue, Interlocked.Increment( ref dataValue ), TransactionPutOptions.NoDuplicateData );
                        }
                        catch ( Exception e )
                        {
                            this._Output.WriteLine( e.ToString() );
                        }
                    }
                }
            } );

            var t2 = Task.Run( () => {
                using ( Benchmark.Run( this._Output, "Write 2", count ) )
                {
                    for ( var i = 1; i < count; i++ )
                    {
                        try
                        {
                            _ = db.Put( keyValue, Interlocked.Increment( ref dataValue ), TransactionPutOptions.NoDuplicateData );
                        }
                        catch ( Exception e )
                        {
                            this._Output.WriteLine( e.ToString() );
                        }
                    }
                }
            } );

            t1.Wait();
            t2.Wait();
        }

    }
}
