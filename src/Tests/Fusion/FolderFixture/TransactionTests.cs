using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

using isr.Lmdb.Fusion.Interop;

using Xunit;
using Xunit.Abstractions;


namespace isr.Lmdb.Fusion.Tests
{

    /// <summary>
    /// A transaction tests validating LMDB Code for tests from the Lightning LMDB implementation .
    /// </summary>
    /// <remarks>   David, 2020-12-29. </remarks>
    [Collection( nameof( FolderCollection ) )]
    public class TransactionTests : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   The output. </summary>
        private readonly ITestOutputHelper _Output;

        /// <summary>   Gets the folder fixture. </summary>
        /// <value> The folder fixture. </value>
        private FolderFixture FolderFixture { get; set; }

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="folderFixture">    The folder fixture. </param>
        /// <param name="output">           The output. </param>
        public TransactionTests( FolderFixture folderFixture, ITestOutputHelper output )
        {
            this.FolderFixture = folderFixture;
            this._Output = output;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="disposing">    True to release both managed and unmanaged resources; false to
        ///                             release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            if ( disposing )
                this.FolderFixture = null;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

        /// <summary>   (Unit Test Method) Transaction should be created. </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        [Fact]
        public void TransactionShouldBeCreated()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunTransactionScenario( ( tx, db ) => {
                Assert.Equal( TransactionState.Active, tx.State );
            } );
        }

        /// <summary>   (Unit Test Method) Transaction should be disposed if environment closes. </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        [Fact]
        public void TransactionShouldBeDisposedIfEnvironmentCloses()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunTransactionScenario( ( tx, db ) => {
                env.Dispose();
                Assert.Equal( TransactionState.Disposed, tx.State );
            } );
        }

        /// <summary>   (Unit Test Method) Transaction should change state on commit. </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        [Fact]
        public void TransactionShouldChangeStateOnCommit()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunTransactionScenario( ( tx, db ) => {
                tx.Commit();
                Assert.Equal( TransactionState.Committed, tx.State );
            } );
        }

        /// <summary>   (Unit Test Method) Child transaction should be created. </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        [Fact]
        public void ChildTransactionShouldBeCreated()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunTransactionScenario( ( tx, db ) => {
                var subTxn = tx.BeginChildTransaction();
                Assert.Equal( TransactionState.Active, subTxn.State );
            } );
        }

        /// <summary>   (Unit Test Method) Resets the transaction disposed on dispose. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ResetTransactionDisposedOnDispose()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunTransactionScenario( ( tx ) => {
                tx.Reset();
                tx.Dispose();
                Assert.Equal( TransactionState.Disposed, tx.State );
            }, beginOptions: TransactionBeginOptions.ReadOnly );
        }

        /// <summary>   (Unit Test Method) Child transaction should be aborted if parent is aborted. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ChildTransactionShouldBeAbortedIfParentIsAborted()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunTransactionScenario( ( tx ) => {
                var child = tx.BeginChildTransaction();
                tx.Abort();
                Assert.Equal( TransactionState.Aborted, child.State );
            } );
        }

        /// <summary>   (Unit Test Method) Child transaction should be aborted if parent is commited. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ChildTransactionShouldBeAbortedIfParentIsCommited()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunTransactionScenario( ( tx ) => {
                var child = tx.BeginChildTransaction();
                tx.Commit();
                Assert.Equal( TransactionState.Aborted, child.State );
            } );
        }

        /// <summary>   (Unit Test Method) Child transaction should be disposed if environment is disposed. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ChildTransactionShouldBeDisposedIfEnvironmentIsDisposed()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunTransactionScenario( ( tx ) => {
                var child = tx.BeginChildTransaction();
                env.Dispose();
                Assert.Equal( TransactionState.Disposed, child.State );
            } );
        }

        /// <summary>   (Unit Test Method) Reads only transaction should change state on reset. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ReadOnlyTransactionShouldChangeStateOnReset()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunTransactionScenario( ( tx ) => {
                tx.Reset();
                Assert.Equal( TransactionState.Reset, tx.State );
            }, beginOptions: TransactionBeginOptions.ReadOnly );
        }

        /// <summary>
        /// (Unit Test Method) Gets the read only transaction should change state on renew.
        /// </summary>
        [Fact]
        public void ReadOnlyTransactionShouldChangeStateOnRenew()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunTransactionScenario( ( tx ) => {
                tx.Reset();
                tx.Renew();
                Assert.Equal( TransactionState.Active, tx.State );
            }, beginOptions: TransactionBeginOptions.ReadOnly );
        }

        /// <summary>   (Unit Test Method) Can count transaction entries. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void CanCountTransactionEntries()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunTransactionScenario( ( tx, db ) => {
                const int entriesCount = 10;
                for ( var i = 0; i < entriesCount; i++ )
                    _ = tx.Put( db, i.ToString(), i.ToString() );

                var count = tx.GetEntriesCount( db );
                Assert.Equal( entriesCount, count );
            } );
        }

        /// <summary>
        /// (Unit Test Method) Can delete previously committed with multiple values by passing null for value.
        /// </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void CanDeletePreviouslyCommittedWithMultipleValuesByPassingNullForValue()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunTransactionScenario( ( tx, db ) => {
                var key = MemoryMarshal.Cast<char, byte>( "abcd" );

                _ = tx.Put( db, key, MemoryMarshal.Cast<char, byte>( "Value1" ) );
                _ = tx.Put( db, key, MemoryMarshal.Cast<char, byte>( "Value2" ), TransactionPutOptions.AppendData );
                tx.Commit();
                tx.Dispose();

                using var delTxn = env.BeginTransaction();
                var result = delTxn.Delete( db, key, null );
                Assert.Equal( NativeResultCode.Success, LmdbException.ToNativeResultCode( result ) );
                delTxn.Commit();
            }, DatabaseOpenOptions.Create | DatabaseOpenOptions.DuplicatesFixed );
        }

        /// <summary>   (Unit Test Method) Transaction should support custom comparer. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void TransactionShouldSupportCustomComparer()
        {
            static int comparison( int l, int r ) => l.CompareTo( r );
            var options = new DatabaseConfiguration { OpenOptions = DatabaseOpenOptions.Create };
            static int compareWith( LmdbBuffer l, LmdbBuffer r ) => comparison( BitConverter.ToInt32( l.CopyToNewArray(), 0 ), BitConverter.ToInt32( r.CopyToNewArray(), 0 ) );
            options.CompareKeyWith( Comparer<LmdbBuffer>.Create( new Comparison<LmdbBuffer>( compareWith ) ) );

            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            using ( var txnT = env.BeginTransaction() )
            using ( var db1 = txnT.OpenDatabase( config: options ) )
            {
                db1.DropThrow( txnT );
                txnT.Commit();
            }

            var txn = env.BeginTransaction();
            var db = txn.OpenDatabase( config: options );

            var keysUnsorted = Enumerable.Range( 1, 10000 ).OrderBy( x => Guid.NewGuid() ).ToList();
            var keysSorted = keysUnsorted.ToArray();
            Array.Sort( keysSorted, new Comparison<int>( comparison ) );

            GC.Collect();
            for ( var i = 0; i < keysUnsorted.Count; i++ )
                _ = txn.Put( db, BitConverter.GetBytes( keysUnsorted[i] ), BitConverter.GetBytes( i ) );

            using var c = txn.CreateCursor( db );
            int order = 0;
            while ( c.Next() == ( int ) NativeResultCode.Success )
                Assert.Equal( keysSorted[order++], BitConverter.ToInt32( c.GetCurrent().key.CopyToNewArray(), 0 ) );
        }

        /// <summary>   (Unit Test Method) Transaction should support custom duplicate sorter. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void TransactionShouldSupportCustomDupSorter()
        {
            static int comparison( int l, int r ) => -Math.Sign( l - r );

            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            var txn = env.BeginTransaction();
            var options = new DatabaseConfiguration { OpenOptions = DatabaseOpenOptions.Create | DatabaseOpenOptions.DuplicatesFixed };
            static int compareWith( LmdbBuffer l, LmdbBuffer r ) => comparison( BitConverter.ToInt32( l.CopyToNewArray(), 0 ), BitConverter.ToInt32( r.CopyToNewArray(), 0 ) );
            options.CompareDataWith( Comparer<LmdbBuffer>.Create( new Comparison<LmdbBuffer>( compareWith ) ) );
            var db = txn.OpenDatabase( config: options );

            var valuesUnsorted = new[] { 2, 10, 5, 0 };
            var valuesSorted = valuesUnsorted.ToArray();
            Array.Sort( valuesSorted, new Comparison<int>( comparison ) );

            using ( var c = txn.CreateCursor( db ) )
                _ = c.Put( BitConverter.GetBytes( 123 ), valuesUnsorted.Select( BitConverter.GetBytes ).ToArray() );

            using ( var c = txn.CreateCursor( db ) )
            {
                int order = 0;

                while ( c.Next() == ( int ) NativeResultCode.Success )
                    Assert.Equal( valuesSorted[order++], BitConverter.ToInt32( c.GetCurrent().value.CopyToNewArray(), 0 ) );
            }
        }

    }
}
