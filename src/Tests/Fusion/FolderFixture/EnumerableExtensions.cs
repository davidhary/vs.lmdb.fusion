using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion.Tests
{
    /// <summary>   A cursor extensions. </summary>
    /// <remarks>   David, 2021-01-12. </remarks>
    public static class EnumerableExtensions
    {

        /// <summary>   Enumerates split in this collection. </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="list">     The list to act on. </param>
        /// <param name="parts">    The parts. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process split in this collection.
        /// </returns>
        public static IEnumerable<IEnumerable<T>> Split<T>( this IEnumerable<T> list, int parts )
        {
            return list
                .Select( ( x, i ) => new { Index = i, Value = x } )
                .GroupBy( x => x.Index / parts )
                .Select( x => x.Select( v => v.Value ) );
        }

    }
}
