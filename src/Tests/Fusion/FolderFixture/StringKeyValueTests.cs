using System;

using Xunit;
using Xunit.Abstractions;

namespace isr.Lmdb.Fusion.Tests
{

    /// <summary>   String Key-Value tests. </summary>
    /// <remarks>   David, 2021-01-13. </remarks>
    [Collection( nameof( FolderCollection ) )]
    public class StringKeyValueTests : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   The output. </summary>
        private readonly ITestOutputHelper _Output;

        /// <summary>   Gets the folder fixture. </summary>
        /// <value> The folder fixture. </value>
        private FolderFixture FolderFixture { get; set;  }

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="folderFixture">    The folder fixture. </param>
        /// <param name="output">           The output. </param>
        public StringKeyValueTests( FolderFixture folderFixture, ITestOutputHelper output )
        {
            this.FolderFixture = folderFixture;
            this._Output = output;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="disposing">    True to release both managed and unmanaged resources; false to
        ///                             release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            if ( disposing )
                this.FolderFixture = null;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

        /// <summary>   (Unit Test Method) Database put should not throw exceptions. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void DatabasePutShouldNotThrowExceptions()
        {
            var key = "key";
            var value = "value";
            using LmdbEnvironment env = new LmdbEnvironment() { DirectoryPath = this.FolderFixture.CreateGuidDirectoryForTest(), MaxDatabases = 2 };
            env.Open();
            using Transaction txn = env.BeginTransaction();
            using Database db = txn.OpenDatabase( config: new DatabaseConfiguration { OpenOptions = DatabaseOpenOptions.Create } );
            _ = txn.Put( db, key, value );
        }

        /// <summary>   (Unit Test Method) Database get should not throw exceptions. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void DatabaseGetShouldNotThrowExceptions()
        {
            using LmdbEnvironment env = new LmdbEnvironment() { DirectoryPath = this.FolderFixture.CreateGuidDirectoryForTest(), MaxDatabases = 2 };
            env.Open();
            using Transaction txn = env.BeginTransaction();
            using Database db = txn.OpenDatabase( config: new DatabaseConfiguration { OpenOptions = DatabaseOpenOptions.Create } );
            _ = txn.Get( db, System.Text.Encoding.UTF8.GetBytes( "key" ) );
        }

        /// <summary>   (Unit Test Method) Database inserted value should be retrieved. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void DatabaseInsertedValueShouldBeRetrieved()
        {
            var key = "key";
            var value = "value";
            using LmdbEnvironment env = new LmdbEnvironment() { DirectoryPath = this.FolderFixture.CreateGuidDirectoryForTest(), MaxDatabases = 2 };
            env.Open();
            using Transaction txn = env.BeginTransaction();
            using Database db = txn.OpenDatabase( config: new DatabaseConfiguration { OpenOptions = DatabaseOpenOptions.Create } );
            _ = txn.Put( db, key, value );
            var persistedValue = txn.Get( db, key );
            Assert.Equal( value , persistedValue );
        }

        /// <summary>   (Unit Test Method) Database delete should remove item. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void DatabaseDeleteShouldRemoveItem()
        {
            var key = "key";
            var value = "value";
            using LmdbEnvironment env = new LmdbEnvironment() { DirectoryPath = this.FolderFixture.CreateGuidDirectoryForTest(), MaxDatabases = 2 };
            env.Open();
            using Transaction txn = env.BeginTransaction();
            using Database db = txn.OpenDatabase( config: new DatabaseConfiguration { OpenOptions = DatabaseOpenOptions.Create } );
            _ = txn.Put( db, key, value );
            txn.Delete( db, key );
            Assert.False( txn.ContainsKey( db, key ) );
        }

        /// <summary>   (Unit Test Method) Database delete should remove all duplicate data items. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void DatabaseDeleteShouldRemoveAllDuplicateDataItems()
        {
            var fs = new FolderFixture();
            using var env = new LmdbEnvironment() {  DirectoryPath = fs.CreateDirectoryForTest(), MapSize = 1024 * 1024, MaxDatabases = 1 };
            env.Open();
            using var txn = env.BeginTransaction();
            using var db = txn.OpenDatabase( config: new DatabaseConfiguration() { OpenOptions = DatabaseOpenOptions.DuplicatesSort } );
            var key = "key";
            var value1 = "value1";
            var value2 = "value2";

            _ = txn.Put( db, key, value1 );
            _ = txn.Put( db, key, value2 );

            txn.Delete( db, key );
            Assert.False( txn.ContainsKey( db, key ) );
        }

        /// <summary>   (Unit Test Method) Contains key should return true if key exists. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ContainsKeyShouldReturnTrueIfKeyExists()
        {
            var key = "key";
            var value = "value";
            using LmdbEnvironment env = new LmdbEnvironment() { DirectoryPath = this.FolderFixture.CreateGuidDirectoryForTest(), MaxDatabases = 2 };
            env.Open();
            using Transaction txn = env.BeginTransaction();
            using Database db = txn.OpenDatabase( config: new DatabaseConfiguration { OpenOptions = DatabaseOpenOptions.Create } );
            _ = txn.Put( db, key, value );
            var exists = txn.ContainsKey( db, key );
            Assert.True( exists );
        }

        /// <summary>   (Unit Test Method) Contains key should return false if key not exists. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ContainsKeyShouldReturnFalseIfKeyNotExists()
        {
            var key = "key";
            using LmdbEnvironment env = new LmdbEnvironment() { DirectoryPath = this.FolderFixture.CreateGuidDirectoryForTest(), MaxDatabases = 2 };
            env.Open();
            using Transaction txn = env.BeginTransaction();
            using Database db = txn.OpenDatabase( config: new DatabaseConfiguration { OpenOptions = DatabaseOpenOptions.Create } );
            Assert.False( txn.ContainsKey( db, key ) );
        }

        /// <summary>   (Unit Test Method) Attempts to get a should return value if key exists. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void TryGetShouldReturnValueIfKeyExists()
        {
            var key = "key";
            var value = "value";
            using LmdbEnvironment env = new LmdbEnvironment() { DirectoryPath = this.FolderFixture.CreateGuidDirectoryForTest(), MaxDatabases = 2 };
            env.Open();
            using Transaction txn = env.BeginTransaction();
            using Database db = txn.OpenDatabase( config: new DatabaseConfiguration { OpenOptions = DatabaseOpenOptions.Create } );
            _ = txn.Put( db, key, value );

            var exists = txn.TryGet( db, key, out string persistedValue );

            Assert.True( exists );
            Assert.Equal( value, persistedValue );
        }

        /// <summary>   (Unit Test Method) Can commit transaction to named database. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void CanCommitTransactionToNamedDatabase()
        {
            var key = "key1";
            var expectedValue = "value";
            var databaseName = "test";
            using LmdbEnvironment env = new LmdbEnvironment() { DirectoryPath = this.FolderFixture.CreateGuidDirectoryForTest(), MaxDatabases = 2 };
            env.Open();
            {
                using Transaction txn = env.BeginTransaction();
                using var db = txn.OpenDatabase( databaseName, new DatabaseConfiguration { OpenOptions = DatabaseOpenOptions.Create } );
                _ = txn.Put( db, key, expectedValue );
                txn.Commit();
            }
            {
                using var txn = env.BeginTransaction();
                using var db = txn.OpenDatabase( databaseName );
                var value = txn.Get( db, key );
                Assert.Equal( expectedValue, value );
            }
        }

    }
}
