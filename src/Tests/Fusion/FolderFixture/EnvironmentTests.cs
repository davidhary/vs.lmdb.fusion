using System;
using System.IO;
using System.Linq;

using Xunit;
using Xunit.Abstractions;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion.Tests
{
    /// <summary>
    /// A environment tests validating LMDB Code for tests from the Lightning LMDB implementation .
    /// </summary>
    /// <remarks>   David, 2020-12-29. </remarks>
    [Collection( nameof( FolderCollection ) )]
    public class EnvironmentTests : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   The output. </summary>
        private readonly ITestOutputHelper _Output;

        /// <summary>   Gets the folder fixture. </summary>
        /// <value> The folder fixture. </value>
        private FolderFixture FolderFixture { get; set; }

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="folderFixture">    The folder fixture. </param>
        /// <param name="output">           The output. </param>
        public EnvironmentTests( FolderFixture folderFixture, ITestOutputHelper output )
        {
            this.FolderFixture = folderFixture;
            this._Output = output;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="disposing">    True to release both managed and unmanaged resources; false to
        ///                             release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            if ( disposing )
                this.FolderFixture = null;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

        #region " HELPER METHODS "

        /// <summary>   Assert environment information. </summary>
        /// <remarks>   David, 2020-12-23. </remarks>
        /// <param name="envInfo">                      Information describing the environment. </param>
        /// <param name="mapAddress">                   The map address. </param>
        /// <param name="mapSize">                      Size of the map. </param>
        /// <param name="maxReaders">                   The maximum readers. </param>
        /// <param name="usedReaders">                  The used readers. </param>
        /// <param name="lastUsedPageId">               Identifier for the last used page. </param>
        /// <param name="lastCommittedTransactionId">   Identifier for the last committed transaction. </param>
        private unsafe void AssertEnvironmentInfo( LmdbEnvironmentInfo envInfo, int mapAddress, long mapSize,
                                                   long maxReaders, long usedReaders, int lastUsedPageId, int lastCommittedTransactionId )
        {
            this._Output.WriteLine( $"{nameof( LmdbEnvironmentInfo.MapAddress )}: {envInfo.MapAddress}" );
            Assert.Equal( mapAddress, envInfo.MapAddress );
            this._Output.WriteLine( $"{nameof( LmdbEnvironmentInfo.MapSize )}:  {envInfo.MapSize}" );
            Assert.Equal( mapSize, envInfo.MapSize );
            this._Output.WriteLine( $"{nameof( LmdbEnvironmentInfo.MaxReaders )}:  {envInfo.MaxReaders}" );
            Assert.Equal( maxReaders, envInfo.MaxReaders );
            this._Output.WriteLine( $"{nameof( LmdbEnvironmentInfo.UsedReaders )}:  {envInfo.UsedReaders}" );
            Assert.Equal( usedReaders, envInfo.UsedReaders );
            this._Output.WriteLine( $"{nameof( LmdbEnvironmentInfo.LastUsedPageId )}:  {envInfo.LastUsedPageId}" );
            Assert.Equal( lastUsedPageId, envInfo.LastUsedPageId );
            this._Output.WriteLine( $"{nameof( LmdbEnvironmentInfo.LastCommittedTransactionId )}:  {envInfo.LastCommittedTransactionId}" );
            Assert.Equal( lastCommittedTransactionId, envInfo.LastCommittedTransactionId );
        }

        #endregion

        /// <summary>   (Unit Test Method) Queries if a given environment should open. </summary>
        /// <remarks>   David, 2021-01-20. </remarks>
        [Fact]
        public void EnvironmentShouldOpen()
        {
            using var env = new LmdbEnvironment();
            env.Open( this.FolderFixture.CreateDirectoryForTest());
            Assert.True( env.IsOpen );
        }

        [Fact]
        public void EnvironmentShouldOpenWithCyrillicPath()
        {
            var path = Path.GetFullPath( Path.Combine( this.FolderFixture.CreateDirectoryForTest(), "МояПапка" ) );
            using LmdbEnvironment env = new LmdbEnvironment();
            env.Open( path );
            Assert.True( env.IsOpen );
        }

        /// <summary>   (Unit Test Method) Environment should close after dispose. </summary>
        /// <remarks>   David, 2021-01-20. </remarks>
        [Fact]
        public void EnvironmentShouldCloseAfterDispose()
        {
            using var env = new LmdbEnvironment();
            env.Open( this.FolderFixture.CreateDirectoryForTest() );
            env.Dispose();
            Assert.False( env.IsOpen );
        }

        /// <summary>   (Unit Test Method) Should lock folder. </summary>
        /// <remarks>   David, 2021-01-09. </remarks>
        [Fact]
        public void EnvironmentShouldLockFolder()
        {
            this._Output.WriteLine( LmdbVersionInfo.LmdbVersionMessage );
            var path = this.FolderFixture.CreateDirectoryForTest();
            {
                using LmdbEnvironment env = new LmdbEnvironment();
                env.Open( path );
                _ = Assert.Throws<System.IO.IOException>( () => Directory.Delete( path, true ) );
            }
        }

        /// <summary>   (Unit Test Method) Should release folder after dispose. </summary>
        /// <remarks>   David, 2021-01-09. </remarks>
        [Fact]
        public void EnvironmentShouldReleaseFolderAfterDispose()
        {
            this._Output.WriteLine( LmdbVersionInfo.LmdbVersionMessage );
            var path = this.FolderFixture.CreateDirectoryForTest();
            {
                using LmdbEnvironment env = new LmdbEnvironment();
                env.Open( path );
            }

            // This should delete the folder if it is not locked.
            Directory.Delete( path, true );
        }

        /// <summary>   (Unit Test Method) Environment should get path. </summary>
        /// <remarks>   David, 2021-01-20. </remarks>
        [Fact]
        public void EnvironmentShouldGetPath()
        {
            using var env = new LmdbEnvironment();
            env.Open( this.FolderFixture.CreateDirectoryForTest() );
            Assert.Equal( env.DirectoryPath, env.GetPath() );
        }

        /// <summary>   Environment should get Cyrillic path. </summary>
        /// <remarks>   David, 2021-01-20. </remarks>
        [Fact]
        public void EnvironmentShouldGetCyrillicPath()
        {
            var path = Path.GetFullPath( Path.Combine( this.FolderFixture.CreateDirectoryForTest(), "МояПапка" ) );
            using var env = new LmdbEnvironment();
            env.Open( path );
            Assert.Equal( path, env.GetPath() );
        }

        /// <summary>   (Unit Test Method) Environment should have no stale readers. </summary>
        /// <remarks>   David, 2021-01-20. </remarks>
        [Fact]
        public void EnvironmentShouldHaveNoStaleReaders()
        {
            using var env = new LmdbEnvironment();
            env.Open( this.FolderFixture.CreateDirectoryForTest() );
            Assert.Equal( 0, env.ReaderCheck() );
        }

        /// <summary>   (Unit Test Method) Environment should get maximum key size. </summary>
        /// <remarks>   David, 2021-01-20. </remarks>
        [Fact]
        public void EnvironmentShouldGetMaxKeySize()
        {
            using var env = new LmdbEnvironment();
            env.Open( this.FolderFixture.CreateDirectoryForTest() );
            Assert.Equal( LmdbEnvironment.DefaultMaxKeySize, env.MaxKeySize );
        }


        /// <summary>   (Unit Test Method) Environment should get page size. </summary>
        /// <remarks>   David, 2021-01-20. </remarks>
        [Fact]
        public void EnvironmentShouldGetPageSize()
        {
            using var env = new LmdbEnvironment();
            env.Open( this.FolderFixture.CreateDirectoryForTest());
            Assert.Equal( LmdbEnvironment.DefaultPageSize, env.PageSize );
        }

        /// <summary>   (Unit Test Method) Environment should get overflow page count. </summary>
        /// <remarks>   David, 2021-01-20. </remarks>
        [Fact]
        public void EnvironmentShouldGetOverflowPageCount()
        {
            using var env = new LmdbEnvironment();
            env.Open( this.FolderFixture.CreateDirectoryForTest() );
            Assert.Equal( 0, env.GetStats().OverflowPages );
        }

        /// <summary>   (Unit Test Method) Environment should get overflow page header size. </summary>
        /// <remarks>   David, 2021-01-20. </remarks>
        [Fact]
        public void EnvironmentShouldGetOverflowPageHeaderSize()
        {
            using var env = new LmdbEnvironment();
            // requires EnvironmentOpenOptions.WriteMap
            env.Open( this.FolderFixture.CreateDirectoryForTest(), EnvironmentOpenOptions.WriteMap );
            Assert.Equal( LmdbEnvironment.DefaultOverflowPageHeaderSize, env.OverflowPageHeaderSize );
        }

        /// <summary>   (Unit Test Method) Environment should open with default information. </summary>
        /// <remarks>
        /// Remark added by David, 2020-12-11. <para>
        /// MapAddr: 0  </para><para>
        /// MapSize: 1048576  </para><para>
        /// MaxReaders: 126  </para><para>
        /// NumReaders: 0  </para><para>
        /// LastPgNo: 1  </para><para>
        /// LastTxnId: 0  </para><para>
        /// </para>
        /// </remarks>
        [Fact]
        public void EnvironmentShouldOpenWithDefaultInfo()
        {
            using var env = new LmdbEnvironment();
            env.Open( this.FolderFixture.CreateDirectoryForTest() );
            var envInfo = env.GetInfo();
            this.AssertEnvironmentInfo( envInfo, 0, LmdbEnvironment.DefaultMapSize, LmdbEnvironment.DefaultMaxReaders, 0, 1, 0 ); //  1048576
        }

        /// <summary>   (Unit Test Method) Environment has disposing events. </summary>
        /// <remarks>   David, 2021-01-19. </remarks>
        [Fact]
        public void EnvironmentHasDisposingEvents()
        {
            using var env = new LmdbEnvironment();
            env.Open( this.FolderFixture.CreateDirectoryForTest() );
            using var tx = env.BeginTransaction( TransactionBeginOptions.None );
            Assert.True( env.HasDisposingEvents );
        }

        /// <summary>   (Unit Test Method) Environment has no disposing events. </summary>
        /// <remarks>   David, 2021-01-19. </remarks>
        [Fact]
        public void EnvironmentHasNoDisposingEvents()
        {
            using var env = new LmdbEnvironment();
            env.Open( this.FolderFixture.CreateDirectoryForTest() );
            using var tx = env.BeginTransaction( TransactionBeginOptions.None );
            tx.Dispose();
            Assert.False( env.HasDisposingEvents );
        }

        /// <summary>   (Unit Test Method) Environment should be created without open options. </summary>
        /// <remarks>   David, 2021-01-11. </remarks>
        [Fact]
        public void EnvironmentShouldBeCreatedWithoutOpenOptions()
        {
            using LmdbEnvironment env = new LmdbEnvironment();
            env.Open( this.FolderFixture.CreateDirectoryForTest() );
        }

        /// <summary>   (Unit Test Method) Environment configuration should be set. </summary>
        /// <remarks>   David, 2021-01-11. </remarks>
        [Fact]
        public void EnvironmentConfigurationShouldBeSet()
        {
            var mapExpected = 1024 * 1024 * 20;
            var maxDatabaseExpected = 2;
            var maxReadersExpected = 3;
            using LmdbEnvironment env = new LmdbEnvironment() { MapSize  = mapExpected, MaxDatabases = maxDatabaseExpected, MaxReaders = maxReadersExpected } ;
            AssertX.Equal( mapExpected, env.MapSize, $"{nameof( LmdbEnvironment.MapSize )}" );
            AssertX.Equal( maxDatabaseExpected, env.MaxDatabases, $"{nameof( LmdbEnvironment.MaxDatabases )}" );
            AssertX.Equal( maxReadersExpected, env.MaxReaders, $"{nameof( LmdbEnvironment.MaxReaders )}" );
            env.Open( this.FolderFixture.CreateDirectoryForTest() );
            AssertX.Equal( mapExpected, env.MapSize, $"{nameof( LmdbEnvironment.MapSize )} after open" );
        }

        /// <summary>   (Unit Test Method) Can get environment information. </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        [Fact]
        public void CanGetEnvironmentInfo()
        {
            var mapExpected = 1024 * 1024 * 200;
            using LmdbEnvironment env = new LmdbEnvironment() { MapSize = mapExpected };
            env.Open( this.FolderFixture.CreateDirectoryForTest() );
            var info = env.GetInfo();
            Assert.Equal( mapExpected, info.MapSize );
        }

        /// <summary>   (Unit Test Method) Maximum databases works through configuration. </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        [Fact]
        public void MaxDatabasesWorksThroughConfig()
        {
            var maxDatabaseExpected = 2;
            using LmdbEnvironment env = new LmdbEnvironment() { MaxDatabases = maxDatabaseExpected };
            env.Open( this.FolderFixture.CreateDirectoryForTest() );
            using ( var tx = env.BeginTransaction() )
            {
                _ = tx.OpenDatabase( "db1", new DatabaseConfiguration { OpenOptions = DatabaseOpenOptions.Create } );
                _ = tx.OpenDatabase( "db2", new DatabaseConfiguration { OpenOptions = DatabaseOpenOptions.Create } );
                tx.Commit();
            }
            Assert.Equal( maxDatabaseExpected, env.MaxDatabases );
        }

        /// <summary>   (Unit Test Method) Can load and dispose multiple environments. </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        [Fact]
        public void CanLoadAndDisposeMultipleEnvironments()
        {
            using LmdbEnvironment env1 = new LmdbEnvironment();
            using LmdbEnvironment env2 = new LmdbEnvironment();
            env1.Dispose();
            env2.Dispose();
        }

        /// <summary>   (Unit Test Method) Environment should be created if read only. </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        [Fact]
        public void EnvironmentShouldBeCreatedIfReadOnly()
        {
            //read only requires environment to have been created at least once before
            string path = this.FolderFixture.CreateDirectoryForTest();
            using ( var env = LmdbEnvironment.OpenNew( path ) )
            {
                env.Dispose();
            }
            using ( var env = new LmdbEnvironment() )
            {
                env.Open( path, EnvironmentOpenOptions.ReadOnly );
                env.Dispose();
            }
        }

        /// <summary>   (Unit Test Method) Environment should be opened. </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        [Fact]
        public void EnvironmentShouldBeOpened()
        {
            using LmdbEnvironment env = new LmdbEnvironment();
            env.Open( this.FolderFixture.CreateDirectoryForTest() );
            Assert.True( env.IsOpen, $"{nameof( LmdbEnvironment.IsOpen )}" );
        }

        /// <summary>   (Unit Test Method) Environment should be closed. </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        [Fact]
        public void EnvironmentShouldBeClosed()
        {
            using LmdbEnvironment env = new LmdbEnvironment();
            {
                env.Open( this.FolderFixture.CreateDirectoryForTest() );
            }
            // The using block is not guaranteed to dispose the object at this point.
            env.Dispose();
            Assert.False( env.IsOpen, $"{nameof( LmdbEnvironment.IsOpen )}" );
        }

        /// <summary>   (Unit Test Method) Environment should be copied. </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        /// <param name="compact">  True to compact. </param>
        [Theory]
        [InlineData( true )]
        [InlineData( false )]
        [CLSCompliant(false)]
        public void EnvironmentShouldBeCopied( bool compact )
        {
            string path = this.FolderFixture.CreateDirectoryForTest();
            string pathCopy = this.FolderFixture.CreateDirectoryForTest( path, "_copy" );
            using LmdbEnvironment env = new LmdbEnvironment();
            {
                env.Open( path );
                env.CopyTo( pathCopy, compact );
            }
            if ( Directory.GetFiles( pathCopy ).Length == 0 )
                Assert.True( false, $"Files copied from {path} to {pathCopy} with {nameof(compact)} = {compact} should exist" );
        }

        /// <summary>   (Unit Test Method) Can open environment more than 50 megabytes. </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        [Fact]
        public void CanOpenEnvironmentMoreThan50Mb()
        {
            using LmdbEnvironment env = new LmdbEnvironment() { MapSize = 55 * 1024 * 1024 };
            env.Open( this.FolderFixture.CreateDirectoryForTest() );
        }

        /// <summary>   (Unit Test Method) could open huge environment. </summary>
        /// <remarks>   David, 2020-12-16.  <para>
        /// LMDB 0.9.70: (December 19, 2015) </para><para>
        /// LmdbEnvironment.Statistics.Entries: 0  </para><para>
        /// LmdbEnvironment.MaxKeySize: 1982 </para><para>
        /// LmdbEnvironment.ReaderCheck = Stale readers count: 0 </para><para>
        /// LmdbEnvironment.MapSize: 2199023255552 </para><para>
        /// </para> </remarks>
        [Fact]
        public void CouldOpenHugeEnvironment()
        {
            string testItemMessage;
            string testItemName;
            this._Output.WriteLine( LmdbVersionInfo.LmdbVersionMessage );
            var path = this.FolderFixture.CreateDirectoryForTest();
            long mapSize = 2 * 1024L * 1024 * 1024 * 1024L;
            using var env = new LmdbEnvironment() { MapSize = mapSize };
            env.Open( path, EnvironmentOpenOptions.WriteMap | EnvironmentOpenOptions.NoSync );

            testItemName = $"{nameof( LmdbEnvironment )}.{nameof( LmdbEnvironment.IsOpen )}";
            testItemMessage = $"{nameof( LmdbEnvironment )} should be open ({testItemName}) after {nameof( LmdbEnvironment.Open )}";
            Assert.True( env.IsOpen, testItemMessage );

            var stat = env.GetStats();
            testItemName = $"{nameof( LmdbEnvironment )}.{nameof( isr.Lmdb.Fusion.Statistics )}.{nameof( isr.Lmdb.Fusion.Statistics.Entries )}";
            this._Output.WriteLine( $"{testItemName}: {stat.Entries}" );
            AssertX.Equal( 0, stat.Entries, testItemName );

            testItemName = $"{nameof( isr.Lmdb.Fusion.LmdbEnvironment )}.{nameof( isr.Lmdb.Fusion.LmdbEnvironment.MaxKeySize )}";
            this._Output.WriteLine( $"{testItemName }: {env.MaxKeySize }" );
            AssertX.Equal( 1982, env.MaxKeySize, testItemName );

            testItemName = $"{nameof( isr.Lmdb.Fusion.LmdbEnvironment )}.{nameof( isr.Lmdb.Fusion.LmdbEnvironment.ReaderCheck )} = Stale readers count";
            this._Output.WriteLine( $"{testItemName}: {env.ReaderCheck() }" );
            int clearedStaleSlots = 0;
            AssertX.Equal( clearedStaleSlots, env.ReaderCheck(), testItemName );

            testItemName = $"{nameof( isr.Lmdb.Fusion.LmdbEnvironment )}.{nameof( isr.Lmdb.Fusion.LmdbEnvironment.MapSize )}";
            this._Output.WriteLine( $"{testItemName}: {env.MapSize }" );
            AssertX.Equal( mapSize, env.MapSize, testItemName );
        }

        /// <summary>   (Unit Test Method) could create many environment. </summary>
        /// <remarks>   David, 2020-12-16. <para>
        /// LmdbEnvironment.GetPath: r:\Spreads\EnvironmentTests\CouldCreateManyEnvironment0 </para><para>
        /// LmdbEnvironment.MapSize: 10737418240 </para><para>
        /// LmdbEnvironment.GetPath: r:\Spreads\EnvironmentTests\CouldCreateManyEnvironment1 </para><para>
        /// LmdbEnvironment.MapSize: 10737418240 </para><para>
        ///  ....  </para><para>
        /// LmdbEnvironment.GetPath: r:\Spreads\EnvironmentTests\CouldCreateManyEnvironment8 </para><para>
        /// LmdbEnvironment.MapSize: 10737418240 </para><para>
        /// LmdbEnvironment.GetPath: r:\Spreads\EnvironmentTests\CouldCreateManyEnvironment9 </para><para>
        /// LmdbEnvironment.MapSize: 10737418240 </para>
        /// Statistics.BTreeDepth: 0 </remarks>
        [Fact]
        public void CouldCreateManyEnvironments()
        {
            string testItemMessage;
            string testItemName;
            long mapSize = 10L * 1024 * 1024 * 1024;
            var basePath = this.FolderFixture.CreateDirectoryForTest();
            this._Output.WriteLine( LmdbVersionInfo.LmdbVersionMessage );
            for ( int i = 0; i < 10; i++ )
            {
                string path = $"{basePath}{i}";
                using LmdbEnvironment env = new LmdbEnvironment {
                    MapSize = mapSize
                };
                env.Open( path );

                testItemMessage = $"Environment {i} should be open after {nameof( LmdbEnvironment.Open )}";
                Assert.True( env.IsOpen, testItemMessage );

                testItemName = $"{nameof( isr.Lmdb.Fusion.LmdbEnvironment )}.{nameof( isr.Lmdb.Fusion.LmdbEnvironment.GetPath )}";
                string environmentPath = env.GetPath();
                this._Output.WriteLine( $"{testItemName}: {environmentPath}" );
                AssertX.Equal( path, environmentPath, testItemName );

                // Map size is updated on each Get.
                long actualMapSize = env.MapSize;
                testItemName = $"{nameof( isr.Lmdb.Fusion.LmdbEnvironment )}.{nameof( isr.Lmdb.Fusion.LmdbEnvironment.MapSize )}";
                this._Output.WriteLine( $"{testItemName}: {actualMapSize}" );
                AssertX.Equal( mapSize, actualMapSize, testItemName );
            }
        }

        /// <summary>
        /// (Unit Test Method) could open multiple environments in the same process.
        /// </summary>
        /// <remarks>   David, 2020-12-16. <para>
        /// Unlike the Spreads Environments, Fusion environments open on the same path are not reference equal.
        /// </para></remarks>
        [Fact]
        public void CouldOpenMultipleEnvironmentsInTheSameProcess()
        {

            string testItemMessage;
            var path = this.FolderFixture.CreateDirectoryForTest();
            var env1 = new LmdbEnvironment();
            env1.Open( path );

            testItemMessage = $"Environment {nameof( env1 )} should be open after {nameof( env1 )}.{nameof( LmdbEnvironment.Open )}";
            Assert.True( env1.IsOpen, testItemMessage );

            var env2 = new LmdbEnvironment();
            env2.Open( path );

            testItemMessage = $"Environment {nameof( env2 )} should be open after {nameof( env2 )}.{nameof( LmdbEnvironment.Open )}";
            Assert.True( env2.IsOpen, testItemMessage );

            testItemMessage = "Two environments opened with the same path should not be reference equal";
            Assert.False( ReferenceEquals( env1, env2 ), testItemMessage );

            env2.Dispose();

            testItemMessage = $"Environment {nameof( env2 )} should not be open after {nameof( env2 )}.{nameof( LmdbEnvironment.Dispose )}";
            Assert.False( env2.IsOpen, testItemMessage );

            testItemMessage = $"Environment {nameof( env1 )} should remain open after {nameof( env2 )}.{nameof( LmdbEnvironment.Dispose )}";
            Assert.True( env1.IsOpen, testItemMessage );

            var env3 = new LmdbEnvironment();
            env3.Open( path );

            testItemMessage = $"Environment {nameof( env3 )} should be open after {nameof( env3 )}.{nameof( LmdbEnvironment.Open )}";
            Assert.False( ReferenceEquals( env1, env3 ), testItemMessage );

            env1.Dispose();
            testItemMessage = $"Environment {nameof( env1 )} should not be open after {nameof( env1 )}.{nameof( LmdbEnvironment.Dispose )}";
            Assert.False( env1.IsOpen, testItemMessage );

            testItemMessage = $"Environment {nameof( env3 )} should remain open after {nameof( env1 )}.{nameof( LmdbEnvironment.Dispose )}";
            Assert.True( env3.IsOpen, testItemMessage );

            env3.Dispose();
            testItemMessage = $"Environment {nameof( env3 )} should not be open after {nameof( env3 )}.{nameof( LmdbEnvironment.Dispose )}";
            Assert.False( env3.IsOpen, testItemMessage );
        }

        /// <summary>   (Unit Test Method) Exception starting transaction before environment open. </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        [Fact]
        public void ExceptionStartingTransactionBeforeEnvironmentOpen()
        {
            using LmdbEnvironment env = new LmdbEnvironment();
            _ = Assert.Throws<InvalidOperationException>( () => env.BeginTransaction() );
        }

        /// <summary>   (Unit Test Method) Commits a transaction. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        [Fact]
        public void CanCommitTransaction()
        {
            using var env = new LmdbEnvironment();
            env.Open( this.FolderFixture.CreateDirectoryForTest() );
            using var tx = env.BeginTransaction( TransactionBeginOptions.None );
            tx.Commit();
        }

        /// <summary>   (Unit Test Method) Transaction can be aborted. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        [Fact]
        public void CanAbortTransaction()
        {
            using var env = new LmdbEnvironment();
            env.Open( this.FolderFixture.CreateDirectoryForTest() );
            using var tx = env.BeginTransaction( TransactionBeginOptions.None );
            tx.Abort();
        }

        /// <summary>   (Unit Test Method) Committed transaction handle should not be allocated. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        [Fact]
        public void CommittedTransactionHandleShouldNotBeAllocated()
        {
            using var env = new LmdbEnvironment();
            env.Open( this.FolderFixture.CreateDirectoryForTest() );
            using var tx = env.BeginTransaction( TransactionBeginOptions.None );
            tx.Commit();
            Assert.False( tx.SafeHandle.IsAllocated );
        }

        /// <summary>   (Unit Test Method) could touch space. </summary>
        /// <remarks>   David, 2020-12-16. <para>
        /// LMDB 0.9.70: (December 19, 2015) </para><para>
        /// Outcomes different than with spreads. Not sure why. </para><para>
        /// LmdbEnvironment.UsedSize: 5259264 (spreads: 5267456) </para><para>
        /// Statistics.BTreeDepth: 1  </para><para>
        /// Touch default: </para><para>
        /// LmdbEnvironment.UsedSize: 5255168 (spreads: 5263360) </para><para>
        /// Statistics.BTreeDepth: 1  </para><para>
        /// </para><para>
        /// The difference between the two values is 4096.  What does that mean?  </para><para>
        /// </para></remarks>
        [Fact]
        public void CouldTouchSpace()
        {
            string testItemMessage;
            string testItemName;
            var path = this.FolderFixture.CreateDirectoryForTest();
            this._Output.WriteLine( LmdbVersionInfo.LmdbVersionMessage );
            {
                using LmdbEnvironment env = new LmdbEnvironment {
                    MapSize = 10 * LmdbEnvironment.DefaultMapSize
                };
                env.Open( path );

                testItemMessage = $"{nameof( LmdbEnvironment )} should be open after {nameof( LmdbEnvironment.Open )}";
                Assert.True( env.IsOpen, testItemMessage );

                var used = env.TouchSpace( 5 );
                testItemName = $"{nameof( LmdbEnvironment )}.{nameof( LmdbEnvironment.UsedSize )}";
                this._Output.WriteLine( $"{testItemName}: {env.UsedSize}" );
                AssertX.Equal( 5259264, env.UsedSize, testItemName );

                testItemName = $"Environment {nameof( Statistics )}.{nameof( Statistics.BTreeDepth )}";
                var stat = env.GetStats();
                this._Output.WriteLine( $"{testItemName}: {stat.BTreeDepth}" );
                AssertX.Equal( 1, stat.BTreeDepth, testItemName );
                env.Dispose();
            }

            path = this.FolderFixture.CreateDirectoryForTest();
            this._Output.WriteLine( "Touch default: " );
            {
                using LmdbEnvironment env = new LmdbEnvironment {
                    MapSize = 10 * LmdbEnvironment.DefaultMapSize
                };
                env.Open( path );

                testItemMessage = $"{nameof( LmdbEnvironment )} should be open after {nameof( LmdbEnvironment.Open )}";
                Assert.True( env.IsOpen, testItemMessage );

                var used = env.TouchSpace();

                testItemName = $"{nameof( LmdbEnvironment )}.{nameof( LmdbEnvironment.UsedSize )}";
                this._Output.WriteLine( $"{testItemName}: {env.UsedSize}" );
                AssertX.Equal( 5255168, env.UsedSize, testItemName );

                testItemName = $"Environment {nameof( Statistics )}.{nameof( Statistics.BTreeDepth )}";
                var stat = env.GetStats();
                this._Output.WriteLine( $"{testItemName}: {stat.BTreeDepth}" );
                AssertX.Equal( 1, stat.BTreeDepth, testItemName );
            }
        }

        /// <summary>   (Unit Test Method) Environment can <see cref="LmdbEnvironment.Write(Action{Transaction})"/>
        /// and <see cref="LmdbEnvironment.Read(Action{Transaction})"/> using <see cref="Cursor.Put(ref LmdbBuffer, ref LmdbBuffer, CursorPutOptions)"/>
        /// and <see cref="Cursor.Get(ref LmdbBuffer, ref LmdbBuffer, CursorGetOption)"/>. </summary>
        /// <remarks>   David, 2021-01-21. </remarks>
        [Fact]
        public void EnvironmentCanWriteReadCursorPutGet()
        {

            string testItemMessage;
            string testItemName;
            this._Output.WriteLine( LmdbVersionInfo.LmdbVersionMessage );
            var path = this.FolderFixture.CreateDirectoryForTest();
            using var env = LmdbEnvironment.OpenNew( path );
            testItemName = $"{nameof( LmdbEnvironment )}.{nameof( LmdbEnvironment.IsOpen )}";
            testItemMessage = $"{nameof( LmdbEnvironment )} should be open ({testItemName}) after {nameof( LmdbEnvironment.Open )}";
            Assert.True( env.IsOpen, testItemMessage );

            using var db = env.OpenDatabase( nameof( BenchmarkTests.CursorGetIntegerKeyValuePairs ), new DatabaseConfiguration( DatabaseOpenOptions.Create ) );

            var values = new byte[] { 1, 2, 3, 4 };

            env.Write( txn => {
                var key = new LmdbBuffer( values );
                var value = new LmdbBuffer( values );
                using var cursor = Cursor.OpenCursor( db, txn );
                Assert.True( 0 == cursor.Put( ref key, ref value, CursorPutOptions.None ), $"Cursor put should succeed" );
                txn.Commit();
            } );

            var count = 10;

            _ = env.Read( txn => {
                var key = new LmdbBuffer( values );
                var value = new LmdbBuffer( values );
                LmdbBuffer value2 = default;
                using var cursor = Cursor.OpenCursor( db, txn );
                for ( int i = 0; i < count; i++ )
                {
                    Assert.True( 0 == cursor.Get( ref key, ref value2, CursorGetOption.SetKey ), $"Cursor get should succeed" );
                }
                cursor.Dispose();
                txn.Dispose();
                Assert.True( values.SequenceEqual( value2.Span.ToArray() ), $"Cursor Get values should equal cursor put values" );
                return true;
            } );


        }



    }
}
