using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace isr.Lmdb.Fusion.Benchmarks
{

    /// <summary>   Values that represent key orderings. </summary>
    /// <remarks>   David, 2021-01-14. </remarks>
    public enum KeyOrdering
    {
        /// <summary>   An enum constant representing the sequential option. </summary>
        Sequential,
        /// <summary>   An enum constant representing the random option. </summary>
        Random
    }

    /// <summary>   A collection of 4 byte key arrays. </summary>
    /// <remarks>   David, 2021-01-14. </remarks>
    public class KeyBatch
    {
        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        /// <param name="buffers">  The buffers. </param>
        private KeyBatch( byte[][] buffers )
        {
            this.Buffers = buffers;
        }

        /// <summary>   Gets the buffers. </summary>
        /// <value> The buffers. </value>
        public byte[][] Buffers { get; }


        /// <summary>   Gets the number of.  </summary>
        /// <value> The count. </value>
        public int Count => this.Buffers.Length;
        /// <summary>   Indexer to get items within this collection using array index syntax. </summary>
        /// <param name="index">    Zero-based index of the entry to access. </param>
        /// <returns>   The indexed item. </returns>
        public ref byte[] this[int index] => ref this.Buffers[index];


        /// <summary>   Generates. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        /// <exception cref="ArgumentException">    Thrown when one or more arguments have unsupported or
        ///                                         illegal values. </exception>
        /// <param name="keyCount">     Number of keys. </param>
        /// <param name="keyOrdering">  The key ordering. </param>
        /// <returns>   A KeyBatch. </returns>
        public static KeyBatch Generate( int keyCount, KeyOrdering keyOrdering )
        {
            var buffers = new byte[keyCount][];

            switch ( keyOrdering )
            {
                case KeyOrdering.Sequential:
                    PopulateSequential( buffers );
                    break;

                case KeyOrdering.Random:
                    PopulateRandom( buffers );
                    break;

                default:
                    throw new ArgumentException( "That isn't a valid KeyOrdering", nameof( keyOrdering ) );
            }

            return new KeyBatch( buffers );
        }

        /// <summary>   Populates a sequential. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        /// <param name="buffers">  The buffers. </param>
        private static void PopulateSequential( byte[][] buffers )
        {
            for ( int i = 0; i < buffers.Length; i++ )
            {
                buffers[i] = CopyToArray( i );
            }
        }

        /// <summary>   Populates a random. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        /// <param name="buffers">  The buffers. </param>
        private static void PopulateRandom( byte[][] buffers )
        {
            var random = new Random( 0 );
            var seen = new HashSet<int>( buffers.Length );

            int i = 0;
            while ( i < buffers.Length )
            {
                var keyValue = random.Next( 0, buffers.Length );

                if ( !seen.Add( keyValue ) )
                    continue;//skip duplicates

                buffers[i++] = CopyToArray( keyValue );
            }
        }

        /// <summary>   Copies to array described by keyValue. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        /// <param name="keyValue"> The key value. </param>
        /// <returns>   A byte[]. </returns>
        private static byte[] CopyToArray( int keyValue )
        {
            var key = new byte[4];
            MemoryMarshal.Write( key, ref keyValue );
            return key;
        }
    }
}
