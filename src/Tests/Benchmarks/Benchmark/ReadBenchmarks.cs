
using BenchmarkDotNet.Attributes;

namespace isr.Lmdb.Fusion.Benchmarks
{
    /// <summary>   A read benchmarks. </summary>
    /// <remarks>   David, 2021-01-14. </remarks>
    [MemoryDiagnoser]
    public class ReadBenchmarks : RWBenchmarksBase
    {
        /// <summary>   Executes the 'setup' operation. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        /// <exception cref="Exception">    Thrown when an exception error condition occurs. </exception>
        public override void RunSetup()
        {
            base.RunSetup();

            //setup data to read
            Transaction transaction = null;
            try
            {
                transaction = this.Env.BeginTransaction();
                for ( int i = 0; i < this.KeyBuffers.Count; i++ )
                {
                    _ = transaction.Put( this.DB, this.KeyBuffers[i], this.ValueBuffer );
                }
                transaction.Commit();
            }
            catch ( System.Exception )
            {
                throw;
            }
            finally
            {
                transaction?.Dispose();
            }
        }

        /// <summary>   Reads this object. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        /// <exception cref="Exception">    Thrown when an exception error condition occurs. </exception>
        [Benchmark]
        public void Read()
        {

            Transaction transaction = null;
            try
            {
                transaction = this.Env.BeginTransaction( beginOptions: TransactionBeginOptions.ReadOnly );
                for ( int i = 0; i < this.OpsPerTransaction; i++ )
                {
                    var (_, _, _) = transaction.Get( this.DB, this.KeyBuffers[i] );
                }
            }
            catch ( System.Exception )
            {
                throw;
            }
            finally
            {
                transaction?.Dispose();
            }

        }
    }
}
