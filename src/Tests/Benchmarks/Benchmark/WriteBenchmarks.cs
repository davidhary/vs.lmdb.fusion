using BenchmarkDotNet.Attributes;

namespace isr.Lmdb.Fusion.Benchmarks
{
    /// <summary>   A write benchmarks. </summary>
    /// <remarks>   David, 2021-01-14. </remarks>
    [MemoryDiagnoser]
    public class WriteBenchmarks : RWBenchmarksBase
    {
        /// <summary>   Writes this object. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        /// <exception cref="Exception">    Thrown when an exception error condition occurs. </exception>
        [Benchmark]
        public void Write()
        {
            Transaction transaction = null;
            try
            {
                transaction = this.Env.BeginTransaction();
                for ( int i = 0; i < this.OpsPerTransaction; i++ )
                {
                    _ = transaction.Put( this.DB, this.KeyBuffers[i], this.ValueBuffer );
                }
                transaction.Commit();

            }
            catch ( System.Exception )
            {
                throw;
            }
            finally
            {
                transaction?.Dispose();
            }
        }
    }
}
