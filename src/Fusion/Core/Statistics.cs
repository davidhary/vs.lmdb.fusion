
namespace isr.Lmdb.Fusion
{

    /// <summary>
    /// Statistics for a database in the environment. 
    /// </summary>
    public class Statistics
    {

        /// <summary>   Statistics for a database or the environment. </summary>
        /// <remarks> Remark added by David, 2020-12-19. </remarks>
        /// <param name="nativeStatistics"> The native statistics. </param>
        internal Statistics( Interop.LmdbStat nativeStatistics )
        {
            this.PageSize = nativeStatistics.PageSize;
            this.BTreeDepth = nativeStatistics.BTreeDepth;
            this.BranchPages = nativeStatistics.BranchPages.ToInt64();
            this.LeafPages = nativeStatistics.LeafPages.ToInt64();
            this.OverflowPages = nativeStatistics.OverflowPages.ToInt64();
            this.Entries = nativeStatistics.Entries.ToInt64();
        }

        /// <summary>
        /// Size of a database page. This is currently the same for all databases. 
        /// </summary>
        public long PageSize { get; }

        /// <summary>
        /// Depth (height) of the B-tree 
        /// </summary>
        public long BTreeDepth { get; }

        /// <summary>
        /// Number of internal (non-leaf) pages 
        /// </summary>
        public long BranchPages { get; }

        /// <summary>
        /// Number of leaf pages 
        /// </summary>
        public long LeafPages { get; }

        /// <summary>
        /// Number of overflow pages 
        /// </summary>
        public long OverflowPages { get; }

        /// <summary>
        /// Number of data items 
        /// </summary>
        public long Entries { get; }

        /// <summary>   Gets the total number of pages. </summary>
        /// <value> The total number of pages. </value>
        public long TotalPages => this.BranchPages + this.LeafPages + this.OverflowPages;

        /// <summary>   Gets the used size. </summary>
        /// <value> The size of the used. </value>
        public long UsedSize => this.PageSize * this.TotalPages;


    }
}
