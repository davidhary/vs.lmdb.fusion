using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{

    /// <summary>   Options for copying an environment. </summary>
    /// <remarks>   Remark added by David, 2020-12-10. </remarks>
    [System.Flags]
    public enum EnvironmentCopyOptions
    {
        /// <summary>   A binary constant representing the none flag. </summary>
        None = 0,

        /// <summary>
        /// Compacting copy: Omit free space from copy, and renumber all pages sequentially.
        /// <see cref="LmdbConstants.MDB_CP_COMPACT"/>
        /// </summary>
        Compact = 0x01
    }
}
