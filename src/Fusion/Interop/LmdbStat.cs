using System;
using System.Runtime.InteropServices;

namespace isr.Lmdb.Fusion.Interop
{

    /// <summary>   Statistics for a database in the environment. </summary>
    /// <remarks> Remark added by David, 2020-12-19. </remarks>
    [StructLayout( LayoutKind.Sequential )]
    internal struct LmdbStat
    {
#pragma warning disable IDE1006 // Naming Styles

        /// <summary>   Size of a database page. This is currently the same for all databases. </summary>
        public uint PageSize;

        /// <summary>
        /// Depth (height) of the B-tree
        /// </summary>
        public uint BTreeDepth;

        /// <summary>
        /// Number of internal (non-leaf) pages
        /// </summary>
        public IntPtr BranchPages;

        /// <summary>
        /// Number of leaf pages
        /// </summary>
        public IntPtr LeafPages;

        /// <summary>
        /// Number of overflow pages
        /// </summary>
        public IntPtr OverflowPages;

        /// <summary>
        /// Number of data items
        /// </summary>
        public IntPtr Entries;

#pragma warning restore IDE1006 // Naming Styles

    }
}
