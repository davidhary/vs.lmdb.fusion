using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{

    /// <summary>   Special options for database put operation. </summary>
    /// <remarks>   Remarked by David, 2020-12-11. </remarks>
    [System.Flags]
    public enum DatabasePutOptions
    {
        /// <summary>
        /// No special behavior.
        /// </summary>
        None = 0,

        /// <summary>
        /// Don't write if the key already exists.
        /// <see cref="LmdbConstants.MDB_NOOVERWRITE"/>
        /// </summary>
        NoOverwrite = 0x10,

        /// <summary>
        /// Don't write if the key and data pair already exist.
        /// <see cref="LmdbConstants.MDB_NODUPDATA"/>
        /// Only for MDB_DUPSORT
        /// </summary>
        NoDuplicateData = 0x20,

        /// <summary>
        /// Just reserve space for data, don't copy it. Return a pointer to the reserved space.
        /// <see cref="LmdbConstants.MDB_RESERVE"/>
        /// </summary>
        ReserveSpace = 0x10000,

        /// <summary>
        /// Data is being appended, don't split full pages.
        /// <see cref="LmdbConstants.MDB_APPEND"/>
        /// </summary>
        AppendData = 0x20000,


        /// <summary>
        /// Duplicate data is being appended, don't split full pages.
        /// <see cref="LmdbConstants.MDB_APPENDDUP"/>
        /// Only for MDB_DUPSORT
        /// </summary>
        AppendDuplicateData = 0x40000


    }
}
