using System;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{
    public partial class Transaction : IDisposable
    {

        /// <summary>
        /// Create a cursor. Cursors are associated with a specific transaction and database and may not
        /// span threads.
        /// </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <param name="db">   A database. </param>
        /// <returns>   The new cursor. </returns>
        public Cursor CreateCursor( Database db )
        {
            return new Cursor( db, this );
        }

        /// <summary>   Gets value from a database. </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="db">   The database to query. </param>
        /// <param name="key">  A span containing the key to look up. </param>
        /// <returns>
        /// Returns a Result Code: A non-zero error value on failure and 0 on success, a
        /// <see cref="LmdbBuffer"/> key and a <see cref="LmdbBuffer"/> Requested value's byte array
        /// corresponding to the key if exists, or null if not.
        /// </returns>
        public unsafe (int resultCode, LmdbBuffer key, LmdbBuffer value) Get( Database db, ReadOnlySpan<byte> key )
        {
            if ( db == null )
                throw new ArgumentNullException( nameof( db ) );

            fixed ( byte* keyBuffer = key )
            {
                var mdbKey = new LmdbBuffer( key.Length, keyBuffer );

                return (SafeNativeMethods.mdb_get( this.Handle, db.Handle, in mdbKey, out var mdbValue ), mdbKey, mdbValue);
            }
        }

        /// <summary>   Gets value from a database. </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <param name="db">   The database to query. </param>
        /// <param name="key">  An array containing the key to look up. </param>
        /// <returns>
        /// Returns a Result Code: A non-zero error value on failure and 0 on success, a
        /// <see cref="LmdbBuffer"/> key and a <see cref="LmdbBuffer"/> Requested value's byte array
        /// corresponding to the key if exists, or null if not.
        /// </returns>
        public (int resultCode, LmdbBuffer key, LmdbBuffer value) Get( Database db, byte[] key )
        {
            return this.Get( db, key.AsSpan() );
        }

        /// <summary>   Puts data into a database. </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="db">       Database. </param>
        /// <param name="key">      Key byte array. </param>
        /// <param name="value">    Value byte array. </param>
        /// <param name="options">  Operation options (optional). </param>
        /// <returns>   A NativeResultCode. </returns>
        public unsafe int Put( Database db, ReadOnlySpan<byte> key, ReadOnlySpan<byte> value, TransactionPutOptions options = TransactionPutOptions.None )
        {
            if ( db == null )
                throw new ArgumentNullException( nameof( db ) );

            fixed ( byte* keyPtr = key )
            fixed ( byte* valuePtr = value )
            {
                var mdbKey = new LmdbBuffer( key.Length, keyPtr );
                var mdbValue = new LmdbBuffer( value.Length, valuePtr );

                return SafeNativeMethods.mdb_put( this.Handle, db.Handle, mdbKey, mdbValue, options );
            }
        }

        /// <summary>   Puts data into a database. </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <param name="db">       The database to query. </param>
        /// <param name="key">      A span containing the key to look up. </param>
        /// <param name="value">    A byte array containing the value found in the database, if it
        ///                         exists. </param>
        /// <param name="options">  Operation options (optional). </param>
        /// <returns>   A NativeResultCode. </returns>
        public int Put( Database db, byte[] key, byte[] value, TransactionPutOptions options = TransactionPutOptions.None )
        {
            return this.Put( db, key.AsSpan(), value.AsSpan(), options );
        }

        /// <summary>   Delete items from a database. </summary>
        /// <remarks>
        /// This function removes key/data pairs from the database. If the database does not support
        /// sorted duplicate data items (MDB_DUPSORT) the data parameter is ignored. If the database
        /// supports sorted duplicates and the data parameter is NULL, all of the duplicate data items
        /// for the key will be deleted. Otherwise, if the data parameter is non- NULL only the matching
        /// data item will be deleted. This function will return MDB_NOTFOUND if the specified key/data
        /// pair is not in the database.
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="db">       A database handle returned by mdb_dbi_open() </param>
        /// <param name="key">      The key to delete from the database. </param>
        /// <param name="value">    The data to delete (optional) </param>
        /// <returns>   A NativeResultCode. </returns>
        public unsafe int Delete( Database db, ReadOnlySpan<byte> key, ReadOnlySpan<byte> value )
        {
            if ( db == null )
                throw new ArgumentNullException( nameof( db ) );

            fixed ( byte* keyPtr = key )
            fixed ( byte* valuePtr = value )
            {
                var mdbKey = new LmdbBuffer( key.Length, keyPtr );
                if ( value == null )
                {
                    return SafeNativeMethods.mdb_del( this.Handle, db.Handle, mdbKey );
                }
                var mdbValue = new LmdbBuffer( value.Length, valuePtr );
                return SafeNativeMethods.mdb_del( this.Handle, db.Handle, mdbKey, mdbValue );
            }
        }

        /// <summary>   Delete items from a database. </summary>
        /// <remarks>
        /// This function removes key/data pairs from the database. If the database does not support
        /// sorted duplicate data items (MDB_DUPSORT) the data parameter is ignored. If the database
        /// supports sorted duplicates and the data parameter is NULL, all of the duplicate data items
        /// for the key will be deleted. Otherwise, if the data parameter is non- NULL only the matching
        /// data item will be deleted. This function will return MDB_NOTFOUND if the specified key/data
        /// pair is not in the database.
        /// </remarks>
        /// <param name="db">   A database handle returned by mdb_dbi_open() </param>
        /// <param name="key">  The key to delete from the database. </param>
        /// <returns>   A NativeResultCode. </returns>
        public unsafe int Delete( Database db, ReadOnlySpan<byte> key )
        {
            fixed ( byte* ptr = key )
            {
                var mdbKey = new LmdbBuffer( key.Length, ptr );
                return SafeNativeMethods.mdb_del( this.Handle, db.Handle, mdbKey );
            }
        }

        /// <summary>   Delete items from a database. </summary>
        /// <remarks>
        /// This function removes key/data pairs from the database. If the database does not support
        /// sorted duplicate data items (MDB_DUPSORT) the data parameter is ignored. If the database
        /// supports sorted duplicates and the data parameter is NULL, all of the duplicate data items
        /// for the key will be deleted. Otherwise, if the data parameter is non- NULL only the matching
        /// data item will be deleted. This function will return MDB_NOTFOUND if the specified key/data
        /// pair is not in the database.
        /// </remarks>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <param name="db">       A database handle returned by mdb_dbi_open() </param>
        /// <param name="key">      The key to delete from the database. </param>
        /// <param name="value">    The data to delete (optional) </param>
        /// <returns>   A NativeResultCode. </returns>
        public int Delete( Database db, byte[] key, byte[] value )
        {
            return this.Delete( db, key.AsSpan(), value.AsSpan() );
        }

        /// <summary>   Delete items from a database. </summary>
        /// <remarks>
        /// This function removes key/data pairs from the database. If the database does not support
        /// sorted duplicate data items (MDB_DUPSORT) the data parameter is ignored. If the database
        /// supports sorted duplicates and the data parameter is NULL, all of the duplicate data items
        /// for the key will be deleted. Otherwise, if the data parameter is non- NULL only the matching
        /// data item will be deleted. This function will return MDB_NOTFOUND if the specified key/data
        /// pair is not in the database.
        /// </remarks>
        /// <param name="db">   A database handle returned by mdb_dbi_open() </param>
        /// <param name="key">  The key to delete from the database. </param>
        /// <returns>   A NativeResultCode. </returns>
        public int Delete( Database db, byte[] key )
        {
            return this.Delete( db, key.AsSpan() );
        }

        /// <summary>   The number of items in the database. </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <param name="db">   The database we are counting items in. </param>
        /// <returns>   The number of items. </returns>
        public long GetEntriesCount( Database db )
        {
            return db.GetEntriesCount( this );
        }

    }
}
