using System.Runtime.CompilerServices;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{
    public partial class Cursor
    {

        #region " PUT "

        /// <summary>   Stores the key value pair by cursor. </summary>
        /// <remarks>
        /// David, 2021-01-16. <para>
        /// This function stores key/data pairs into the database. The cursor is positioned at the new
        /// item, or on failure usually near it. Note: Earlier documentation incorrectly said errors
        /// would leave the state of the cursor unchanged. If the function fails for any reason, the
        /// state of the cursor will be unchanged. If the function succeeds and an item is inserted into
        /// the database, the cursor is always positioned to refer to the newly inserted item.
        /// </para>
        /// </remarks>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="value">    [out] The value. </param>
        /// <param name="options">  Options for controlling the operation. </param>
        /// <returns>   A non-zero error value on failure and 0 on success. </returns>
        public int Put( ref LmdbBuffer key, ref LmdbBuffer value, CursorPutOptions options )
        {
            return SafeNativeMethods.mdb_cursor_put( this.Handle, ref key, ref value, options );
        }

        /// <summary>   Stores the key value pair by cursor. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. <para>
        /// Validates writable handle. </para> </remarks>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="value">    [out] The value. </param>
        /// <param name="options">  Options for controlling the operation. </param>
        /// <returns>   A non-zero error value on failure and 0 on success. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public int PutSafe( ref LmdbBuffer key, ref LmdbBuffer value, CursorPutOptions options )
        {
            return LmdbException.AssertExecute( SafeNativeMethods.mdb_cursor_put( this.AssertWritableHandle(), ref key, ref value, options ) );
        }

        /// <summary>   Stores the key value pair by cursor. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. <para>
        /// Validates writable handle. </para> </remarks>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="value">    [in,out] The value. </param>
        /// <param name="options">  Options for controlling the operation. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void PutThrow( ref LmdbBuffer key, ref LmdbBuffer value, CursorPutOptions options )
        {
            _ = LmdbException.AssertExecute( SafeNativeMethods.mdb_cursor_put( this.AssertWritableHandle(), ref key, ref value, options ) );
        }

        #endregion

        #region " ADD, APPEND, REPLACE "

        /// <summary>   Adds a key-value pair. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="value">    [in,out] The value. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public int Add( ref LmdbBuffer key, ref LmdbBuffer value )
        {
            return SafeNativeMethods.mdb_cursor_put( this.Handle, ref key, ref value, CursorPutOptions.NoOverwrite );
        }

        /// <summary>   Replaces a key-value pair. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="value">    [in,out] The value. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public int Replace( ref LmdbBuffer key, ref LmdbBuffer value )
        {
            return SafeNativeMethods.mdb_cursor_put( this.Handle, ref key, ref value, CursorPutOptions.Current );
        }

        /// <summary>   Appends a key. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="value">    [out] The value. </param>
        /// <param name="dup">      (Optional) True to duplicate. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public int Append( ref LmdbBuffer key, ref LmdbBuffer value, bool dup = false )
        {
            return SafeNativeMethods.mdb_cursor_put( this.Handle, ref key, ref value, dup ? CursorPutOptions.AppendDuplicateData : CursorPutOptions.AppendData );
        }

        #endregion

    }
}
