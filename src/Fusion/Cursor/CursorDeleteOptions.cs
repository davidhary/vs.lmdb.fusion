using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{

    /// <summary>   Cursor delete operation options fro both single- and multi-valued key cursors. </summary>
    /// <remarks> Remark added by David, 2020-12-16. </remarks>
    [System.Flags]
    public enum CursorDeleteOption
    {
        /// <summary>
        /// No special behavior
        /// </summary>
        None = 0,

        /// <summary>
        /// Remove all duplicate data items.
        /// Only for<see cref="DatabaseOpenOptions.DuplicatesSort"/> <see cref="LmdbConstants.MDB_DUPSORT"/>.
        /// <see cref="LmdbConstants.MDB_NODUPDATA"/>
        /// </summary>
        NoDuplicateData = 0x20
    }


}
