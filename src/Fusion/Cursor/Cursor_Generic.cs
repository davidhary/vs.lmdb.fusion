using System.Runtime.CompilerServices;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{
    public partial class Cursor
    {

        /// <summary>   Attempts to get. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <typeparam name="TKey">     Type of the key. </typeparam>
        /// <typeparam name="TValue">   Type of the value. </typeparam>
        /// <param name="key">          [in,out] The key. </param>
        /// <param name="value">        [out] The value. </param>
        /// <param name="operation">    The operation. </param>
        /// <returns>   A non-zero error value on failure and 0 on success. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public unsafe int Get<TKey, TValue>( ref TKey key, ref TValue value, CursorGetOption operation )
            where TKey : struct where TValue : struct
        {
            var keyPtr = Unsafe.AsPointer( ref key );
            long size = System.Runtime.CompilerServices.Unsafe.SizeOf<TKey>();
            var key1 = new LmdbBuffer( size, ( byte* ) keyPtr );

            var valuePtr = Unsafe.AsPointer( ref value );
            size = System.Runtime.CompilerServices.Unsafe.SizeOf<TValue>();
            var value1 = new LmdbBuffer( size, ( byte* ) valuePtr );

            int ret = this.Get( ref key1, ref value1, operation );
            if ( 0 == ret )
            {
                key = Unsafe.ReadUnaligned<TKey>( key1.Data );
                value = Unsafe.ReadUnaligned<TValue>( value1.Data );
            }
            return ret;
        }

        /// <summary>   Puts the key value pair. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <typeparam name="TKey">     Type of the key. </typeparam>
        /// <typeparam name="TValue">   Type of the value. </typeparam>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="value">    [out] The value. </param>
        /// <param name="options">  Options for controlling the operation. </param>
        /// <returns>   A non-zero error value on failure and 0 on success. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public unsafe int Put<TKey, TValue>( ref TKey key, ref TValue value, CursorPutOptions options )
            where TKey : struct where TValue : struct
        {
            var keyPtr = Unsafe.AsPointer( ref key );
            long size = System.Runtime.CompilerServices.Unsafe.SizeOf<TKey>();
            var key1 = new LmdbBuffer( size, ( byte* ) keyPtr );

            var valuePtr = Unsafe.AsPointer( ref value );
            size = System.Runtime.CompilerServices.Unsafe.SizeOf<TValue>();
            var value1 = new LmdbBuffer( size, ( byte* ) valuePtr );

            int ret = this.Put( ref key1, ref value1, options );
            if ( 0 == ret )
            {
                key = Unsafe.ReadUnaligned<TKey>( key1.Data );
                value = Unsafe.ReadUnaligned<TValue>( value1.Data );
            }
            return ret;
        }

    }

}
